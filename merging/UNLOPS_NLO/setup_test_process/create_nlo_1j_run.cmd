#
# mg5_aMC run script to generate SM p p > t t~ at NLO accuracy
#
# process + one extra hard jet at NLO accuracy
generate p p > t t~ [QCD]
add process p p > t t~ j [QCD]
# output directory
output tt_dilept_nlo_sm_1j_fixed_central
