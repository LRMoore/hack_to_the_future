import model TopSpin_EFT_UFO
generate p p > t t~ QED=0 QCD=99 NP=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0)
add process p p > t t~ j QED=0 QCD=99 NP=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0)
#add process p p > t t~ j j QED=0 QCD=99 NP=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0)
output tt_dilept_lo_eft_1j_all
