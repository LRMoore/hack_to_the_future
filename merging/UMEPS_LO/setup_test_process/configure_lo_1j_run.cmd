# use as:  ./bin/madevent setup_100k_events_umeps.cmd
#
# move to card editing mode (configure run, but don't generate events yet)
edit_cards
# set auto-shower off so can run pythia separately
shower=OFF
# set detector sim off
detector=OFF
# set madanalysis off
analysis=OFF
# set reweight on so can regenerate samples with varied param choices
reweight=OFF
# move to editing param/runcards
done
#
set run_tag tt_dilept_lo_eft_umeps_1j_100k
# set 100k unweighted_events
set nevents 100000
# turn off systematics studies (will use NLO systematics from NLO SM later)
set use_syst False
# will do CKKW-L/UMEPS merging, so switch off ickkw in run_card to 0 (see e.g. 'Merging in Madgraph 5 and Pythia 8, a brief overview')
set ickkw 0
# set minimum parton kT relative to beam to be unrestricted at matrix element level before generating parton events
set xqcut 0
# set minimum jet pt to the UMEPS merging scale (10 GeV)
set ptj 10
# exit setting up Cards
done
