# !/usr/bin/env/python
# -*- coding: utf-8 -*-

"""
#./shower_and_analyse_fifo.py <shower_config_opt1> <dir_with_lhes1> (<shower_config_opt2> <dir_with_lhes2>...) <other options>

#e.g. :

#./shower_and_analyse_fifo.py --umeps_lo_1j_dir ./umeps_lo_1j_events/<correspoding lhes in here> --num_cores=16

#Run Pythia 8 shower with UNLOPS/UMEPS merging on appropriately generated (N)LO
#p p> t t~ > l+ l- vl vl~ b b~ LHEs, and pipe output to rivet analysis.

"""

from manip_run_dirs import *

#---------------------------------------------------
#   parse command line args for run directories etc
#--------------------------------------------------

parser = argparse.ArgumentParser(description=__doc__)
#description=__doc__)

group = parser.add_mutually_exclusive_group()

# printout options
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")

# specify directories containing LHEs
parser.add_argument('-um1','--umeps_lo_1j_dirs', dest='umeps_lo_1j_dirs', nargs='*',default=[],type=str,
help='a string specifying the path to a directory containing LO tt(+j) LHEFs for UMEPS merging')
parser.add_argument('-um2','--umeps_lo_2j_dirs', dest='umeps_lo_2j_dirs', nargs='*',default=[],type=str,
help='a string specifying the path to a directory containing LO tt(+jj) LHEFs for UMEPS merging')
parser.add_argument('-un1','--unlops_nlo_1j_dirs', dest='unlops_nlo_1j_dirs', nargs='*',default=[],type=str,
help='a string specifying the path to a directory containing NLO tt(+j) LHEFs for UNLOPS merging')
# leave this out for now - need to then specify >=2 LHEs simultaneously for nj NLO+ mj LO merging
#parser.add_argument('-un2','--unlops_lo_2j_dirs', dest='unlops_lo_2j_dirs', nargs='*',default=[],type=str,
#help='a string specifying the path to a directory containing LO ttjj LHEFs for UNLOPS merging')

# specify demands to cluster
parser.add_argument('-ivy','--ivy-only', action="store_true", default=False, dest='ivy_only',help='boolean which can be set to True to request ivy bridge nodes only (NLO merging doesn\'t work with current compiler flags on older nodes)')
parser.add_argument('-tj','--job-time', dest='job_time', type=str, default='0-16:00', help='est. completion time per job in d-hh:mm')
parser.add_argument('-mj','--job-mem', dest='job_mem', type=str, default='2048', help='max memory requested per job in MB')
#parser.add_argument('-nc','--num-cores', type=int, default=20,
#                    help='number of cores requested from each cluster node')
#parser.add_argument('-nn','--num-nodes', type=int, default=1,
#                    help='number of nodes requested from cluster')

# pythia executable path
parser.add_argument('-ex','--exe-path', type=str, dest='exec_path', default = '/home/ucl/cp3/lmoore/MG5_aMC_v2_6_0/examples/main89',
                    help = 'absolute path to pythia executable (e.g. main89) to supply with .cmnd card')

# specify rivet analysis to feed events to
parser.add_argument('-a','--analysis-name', type=str, dest='ana_name', default='tt_Pol_Ana',
                    help='name of rivet analysis to feed pythia events to through hepmc fifo') # remember to source env in bash cmds

# default muR/ muF scales to enforce in merging if lhef set to dynamic
parser.add_argument('-muR','--default-muR', type=float, dest='default_muR', default=172.5, help = 'default (fixed) renormalisation scale for merging in .cmnd card')
parser.add_argument('-muF','--default-muF', type=float, dest='default_muF', default=172.5, help = 'default (fixed) factorisation scale for merging in .cmnd card')

# overwrite existing yoda files or skip
parser.add_argument('-ov','--overwrite', action="store_true", default=False, help='don\'t skip showering/analysing lhes in directories where .yoda already exists')

# limit to only first N runs encountered (for e.g. testing purposes)
parser.add_argument('-nr','--num-runs', dest='num_runs', type=int, default=0,help='only run shower + analysis on first n run directories encountered')

# choose whether to merge yodas at the end
parser.add_argument('-m','--merge-yodas', action='store_true', default=False,
                    help='for each distinct set of showered events, choose whether to merge the resulting YODAs from running the analysis')

# manual parallel mode
parser.add_argument('-mp','--manual-parallel', action='store_true', dest='do_mp', default=False, help='run manually on cores set by -npc option')
parser.add_argument('-mpc','--man-parallel-cores', type=int, dest='mp_cores', default=20, help='number of cores for manual parallel')
parser.add_argument('-mpd','--man-parallel-dir', type=str, dest='man_parallel_dir', default='./manual_parallel_runs', help='directory name for manual job parallelisation')

# parse options
args = parser.parse_args()



#------------------------------------------------------------------------------
# body of script
#------------------------------------------------------------------------------

#
## print out directory info for each type of pre-merging sample if -v flag passed
if args.verbose:
        print '{} called with arguments:\n'.format(__file__)
        print '######################\n'
        print 'UMEPS LO 1j source directories: '
        print map( lambda x: '{} '.format(x), args.umeps_lo_1j_dirs), '\n'
        print 'UMEPS LO 2j source directories: '
        print map( lambda x: '{} '.format(x), args.umeps_lo_2j_dirs), '\n'
        print 'UNLOPS NLO 1j source directories: '
        print map( lambda x: '{} '.format(x), args.unlops_nlo_1j_dirs), '\n'
        #print 'UNLOPS LO 2j directories: '
        #print map( lambda x: '{} '.format(x), args.unlops_lo_2j_dirs)
        print '\n'
##
#

#
## exit if invalid arguments supplied to script (not lists of directories containing LHEFs)
for dir_list in [args.umeps_lo_1j_dirs, args.umeps_lo_2j_dirs, args.unlops_nlo_1j_dirs]:#, args.unlops_lo_2j_dirs]:
    if any(os.path.isdir(item) == False for item in dir_list):
        print dir_list, 'is not a valid list of directories, exiting...'
        sys.exit(1)
        ##
        #
##
#

# form lists of full paths to run_... directories in appropriate LO/NLO formats within directories given to script
um1_run_dirs = flattenList( map( lambda d: getAbsolutePaths(d,include_files=False,dir_pattern=lo_lhe_dir_regex), args.umeps_lo_1j_dirs ))
um2_run_dirs = flattenList( map( lambda d: getAbsolutePaths(d,include_files=False,dir_pattern=lo_lhe_dir_regex), args.umeps_lo_2j_dirs ))
un1_run_dirs = flattenList( map( lambda d: getAbsolutePaths(d,include_files=False,dir_pattern=nlo_lhe_dir_regex), args.unlops_nlo_1j_dirs ))
#un2_run_dirs = flattenList( map( lambda d: getAbsolutePaths(d,include_files=False,dir_pattern=lo_lhe_dir_regex), args.unlops_lo_2j_dirs ))

if args.verbose:
    print '\nIdentified UMEPS LO 1j run directories:\n ', um1_run_dirs
    print '\nIdentified UMEPS LO 2j run directories:\n ', um2_run_dirs
    print '\nIdentified UNLOPS NLO 1j run directories:\n ', un1_run_dirs
    #print '\nunlops 2j run directories:\n ', un2_run_dirs
    print '\n'
#


# copy appropriately configured pythia .cmnd cards to each type of run directory
map( lambda x: copyCardToLHEFDir('{}/{}'.format(cmnd_cards_path,cmnd_card_umeps_lo_1j), x, args.default_muR, args.default_muF) , um1_run_dirs)
map( lambda x: copyCardToLHEFDir('{}/{}'.format(cmnd_cards_path,cmnd_card_umeps_lo_2j), x, args.default_muR, args.default_muF) , um2_run_dirs)
map( lambda x: copyCardToLHEFDir('{}/{}'.format(cmnd_cards_path,cmnd_card_unlops_nlo_1j), x, args.default_muR, args.default_muF) , un1_run_dirs)
#map( lambda x: copyCardToLHEFDir('{}/{}'.format(cmnd_cards_path,cmnd_card_unlops_lo_2j, default_muR, default_muF), x) , un2_run_dirs)

# all run directories found for all the merging cases
all_run_dirs = um1_run_dirs + um2_run_dirs + un1_run_dirs#+un2_run_dirs

# generate a list of the specific path arguments needed by each shower+analyse command as a list of 5-element lists
cmnd_path_list_base = map(lambda x: [args.exec_path, args.ana_name, runFIFOPath(x), runCmndPath(x), runYodaPath(x)], all_run_dirs)

#
## if overwrite .yoda file flag set to False, ignore shower+analyse commands for directories where .yoda files already exist
if not args.overwrite:

    # filter out commands for which the corresponding absolute path to .yoda file does not already exist
    print 'Checking for existing .yoda files...'
    cmnd_path_list = filter( lambda x: os.path.isfile(x[4]) == False, cmnd_path_list_base)

    # get the complement of the original list of all .yoda files involved and those which don't yet exist
    existing_yodas = list(set((map(lambda x: x[4], cmnd_path_list_base))) - set((map(lambda x: x[4], cmnd_path_list))))

    # print message displaying those skipped
    if existing_yodas == []:
        print 'No existing .yoda files found.'
    else:
        print 'Skipping directories with existing yoda files:\n\n{}'.format(existing_yodas)

# otherwise just use the full list regardless of whether they have already been generated
else:

    print 'Overwriting existing .yoda files...'
    cmnd_path_list = cmnd_path_list_base


##
#

#
## if the number of runs is restricted by the option supplied to the script, only run shower+analysis on the first nr LHEFs
if args.num_runs > 0 and args.num_runs < len(cmnd_path_list):

    print 'Will run shower + analysis for only the first {} LHEF directories...'.format(args.num_runs)
    cmnd_path_list = cmnd_path_list[0:(args.num_runs)]

elif args.num_runs > 0 and args.num_runs >= len(cmnd_path_list):

    print '{} runs requested, but only {} LHEF directories identified. Will run shower + analysis for all...'.format(args.num_runs, len(cmnd_path_list))

else:

    print 'Will run shower + analysis for all {} LHEF directories...'.format(len(cmnd_path_list))
##
#


#print cmnd_path_list

# don't need this separate bash file as feeding the paths in bash commands directly to slurm in this script

# if flagged manual parallel, create a directory with a script to split up the jobs
if args.do_mp:

    print 'In manual parallel mode...'
    # make groups of bash commands for each job for manual parallelisation
    run_command_sets = map(lambda x: buildRunCommands(*x), cmnd_path_list)

    # create a directory for bash scripts from args
    #mp_directory = os.path.dirname(args.man_parallel_dir)
    if not os.path.exists(args.man_parallel_dir):
   	print 'Creating directory {} for manual parallel job bash scripts...'.format(args.man_parallel_dir)
    	os.makedirs(args.man_parallel_dir)#mp_directory)
    else:
    	print 'Directory {} already exists...'.format(args.man_parallel_dir)


    # move into manual parallel directory containing sets of scripts for passing squeue + jobthreading.py
    with cd(args.man_parallel_dir):

        # create n subdirectories by dividing total number of jobs by number of cores asked for simultaneously
        # ceiling div
        n_dirs = -(-len(run_command_sets) // args.mp_cores)
        # list of dir suffixes
        dir_suffs = range(n_dirs)

        # divide the command sets into this number of sublists
        split_command_sets = [run_command_sets[i:i + args.mp_cores] for i in xrange(0, len(run_command_sets), args.mp_cores)]

        # create ndir directory names with suffixes _0, _1 etc
        subdir_names = map(lambda x: 'runscripts_set_{}'.format(x), dir_suffs)
	#print os.path.dirname('runscripts_set_{}'.format(0))
	#print subdir_names

        print 'Creating {} run directories for {} jobs over {} cores...'.format(n_dirs,len(run_command_sets),args.mp_cores)
        # create these directories if they don't already exist
        for subdir in subdir_names:
            if not os.path.exists(subdir):
                os.makedirs(subdir)

        # dispatch command sublists into each
        for i in dir_suffs:

            with cd(subdir_names[i]):
                n_subruns = len(split_command_sets[i])
		#print split_command_sets[i]
                print 'Creating {} subrun scripts in {}...'.format(n_subruns,subdir_names[i])
                subrun_suffs = range(n_subruns)
		#print subrun_suffs
                subrun_filenames = map(lambda x: "subrun_{}.srun".format(x), subrun_suffs)

                # write each individual job command set to one file
                for subrun_no in subrun_suffs:
                    with open(subrun_filenames[subrun_no], "w") as subrun_file:
                        print "Writing commands to file: {}".format(subrun_filenames[subrun_no])
                        # overwrite if already exists
                        subrun_file.seek(0)
                        # sequentially write each command in the single job line by line
                        map(lambda x: subrun_file.write(x), split_command_sets[i][subrun_no])
                        subrun_file.truncate()

                    # make an input text file for jobthreading
                    with open('batch_commands.txt', "w") as subrun_commands:
                        print "Writing input command file {}/batch_commands.txt for jobthreading.py...".format(subdir_names[i])
                        subrun_commands.seek(0)
                        # write a set of lines like ./subrun_1.srun, ./subrun_2.srun... to a file batch_commands.txt in the subrun dir
                        map(lambda x: subrun_commands.write('./{}\n'.format(x)), subrun_filenames)
			#print subrun_filenames
                    #
                    with open('run_batch.srun', "w") as slurm_script:
                        print "Writing script {}/run_batch.srun for feeding slurm_submit...".format(subdir_names[i])

                        cmd_txt = """#!/bin/bash

#SBATCH --job-name=test
#SBATCH --output=test.out
#SBATCH --export=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu={mj}

./jobthreading.py {cmnd_file} {nj} {nc}""".format(mj = args.job_mem, cmnd_file='batch_commands.txt',nj=n_subruns,nc=n_subruns)

                        # overwrite if already exists
                        slurm_script.seek(0)
                        # write command text to file
                        slurm_script.write(cmd_txt)
                        slurm_script.truncate()

        #

        #raise Exception("cd back")


#



'''
# write to file
output_commands = "MC_analysis_commands.srun"
print 'Writing commands to file: {}'.format(output_commands)
with open(output_commands, "w") as command_file:
    map( lambda x: command_file.write(x), flattenList(shower_cmnd_list) )
'''
#print '\n\nCommands:\n\n'

#for cmnd in cmnd_path_list:
#	print cmnd, '\n'

#os.exit()
#map(lambda x: print 'commands:\n\n{}'.format(x), cmnd_path_list)

#------------------------------------------------------------------------------
# feed these commands to slurm batch manager as individual jobs
#------------------------------------------------------------------------------

# this will only work on cluster where slurm_utils is installed
from CP3SlurmUtils.Configuration import Configuration
from CP3SlurmUtils.SubmitWorker import SubmitWorker
from CP3SlurmUtils.Exceptions import CP3SlurmUtilsException

config = Configuration()

#--------------------------------------------------------------------------------
# 1. SLURM sbatch command options
#--------------------------------------------------------------------------------

config.sbatch_partition = 'cp3'
config.sbatch_qos = 'cp3'
config.sbatch_workdir = '.'
# approx time for one job (guess - update this once have a better idea)
config.sbatch_time = args.job_time
config.sbatch_mem = args.job_mem
config.sbatch_output = '/dev/null'
config.sbatch_error = '/dev/null'
config.sbatch_additionalOptions = ['--mail-type=ALL', '--mail-user=l.moore@cern.ch', '--cpus-per-task=2']

# if request only ivy bridge nodes, add this to the slurm options
if args.ivy_only:
    ivy_nodes_string = ",".join(ivy_bridge_nodes)
    ivy_option_string = '-w {}'.format(ivy_nodes_string)
    print("Adding option for ivy bridge nodes:\n {}".format(ivy_option_string))
    config.sbatch_additionalOptions.append(ivy_option_string)

#--------------------------------------------------------------------------------
# 2. User batch script parameters that are same for all jobs
#--------------------------------------------------------------------------------

config.environmentType = ''
config.cmsswDir = ''

# not using an input sandbox since have absolute paths to files on scratch
config.inputSandboxContent = []
config.inputSandboxDir = ''
config.inputSandboxFilename = ''

# shouldn't need to alter this - where the slurm batch script generated by feeding this config file to slurm_submit -s will go
config.batchScriptsDir = config.sbatch_workdir + '/slurm_batch_scripts'
config.batchScriptsFilename = ''

# log files automatically should be written to sbatch_workdir
config.stageout = True
# don't need to copy back any files
config.stageoutFiles = []

# We chose the filename of the outputs to be independent of the job array id number (but dependent on the job array task id number).
# So let's put the output files in a directory whose name contains the job array id number,
# so that each job array we may submit will write in a different directory.
config.stageoutDir = config.sbatch_workdir + '/slurm_outputs/job_array_${SLURM_ARRAY_JOB_ID}'

config.writeLogsOnWN = True
config.separateStdoutStderrLogs = False
config.stdoutFilename = ''
config.stderrFilename = ''
config.stageoutLogs = True
# The default filename of the slurm logs has already a job array id number and a job array task id number in it.
# So we can put all logs together (even from different job arrays we may submit) in a unique directory; they won't overwrite each other.
config.stageoutLogsDir = config.sbatch_workdir + '/slurm_logs'

# bundle jobs together under an array specified by 4 digit ID, and writes just one batch script for slurm for this array
config.useJobArray = True

# num jobs will be submitted of the length of the config parameter 'inputParams', should == no. requested LHE directories processed
config.numJobs = None

#--------------------------------------------------------------------------------
# 3 Job-specific input parameters and payload
#--------------------------------------------------------------------------------

# ordered list of variable names in each sublist of arguments to be run
config.inputParamsNames = ['executable', 'ana_name', 'fifo_path', 'cmnd_path', 'yoda_path']
# list of the lists of specific arguments named above
config.inputParams = cmnd_path_list

# the command set to be run for each job in the payload
config.payload = \
"""
echo "Start of user payload for job ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
mkfifo ${fifo_path}
${executable} ${cmnd_path} ${fifo_path} &
rivet -a ${ana_name} -H ${yoda_path} ${fifo_path}
echo "${yoda_path}\n" >> /home/ucl/cp3/lmoore/yoda_paths.log
echo "  End of user payload for job ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
"""

''' EXAMPLE output:


Would submit a job array consisting of 20 jobs.
Input parameters to the job's payload for the first 10 jobs:
Job 1:
    executable = /home/ucl/cp3/lmoore/MG5_aMC_v2_6_0/examples/main89
    ana_name = tt_Pol_Ana
    fifo_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_21/run_21_fifo.hepmc
    cmnd_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_21/tt_dilept_lo_eft_umeps_1j.cmnd
    yoda_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_21/run_21.yoda
Job 2:
    executable = /home/ucl/cp3/lmoore/MG5_aMC_v2_6_0/examples/main89
    ana_name = tt_Pol_Ana
    fifo_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_56/run_56_fifo.hepmc
    cmnd_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_56/tt_dilept_lo_eft_umeps_1j.cmnd
    yoda_path = /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/Events/run_56/run_56.yoda

'''

#print config.payload

#--------------------------------------------------------------------------------
# 4 Submit job using slurm configuration object
#--------------------------------------------------------------------------------

try:
    os.system("env > /nfs/user/lmoore/MG5_aMC_v2_6_0/tt_dilept_lo_eft_1j/env_submit.txt")
    # submit job
    submitWorker = SubmitWorker(config,submit = True)
    submitWorker()

except CP3SlurmUtilsException as ex:
    print ex
