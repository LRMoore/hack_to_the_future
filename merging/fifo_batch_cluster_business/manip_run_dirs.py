#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import yoda
import shutil
import glob
import operator
import itertools
# needed for enhanced regex functionality; sudo pip install regex
#import regex as re
import re
import argparse
import fileinput
from contextlib import contextmanager

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

''' rewrite this with classes later e.g.
#
##
class runDirectory:
    """ Class containing information and functions to manipulate files within a MG5_aMC@NLO
    directory containing .lhe(.gz) files such as '.../run_01/'. """

    def __init__(self, abs_path, LHE_file ):
        ...
##
#
'''

#------------------------------------------------------------------------------
# specify paths to executables, cmnd templates, global regex patterns used etc
#------------------------------------------------------------------------------

# rivet analysis used
rivet_analysis_name = 'tt_Pol_Ana'

# default muR / muF to force in merging (used since manually reweighted LHEs back from fixed scale to 300)
#default_muR = 300
#default_muF = 300

# regexes to match madspin decayed run directories 'run_01_decayed_1' etc for nlo and regular run directories 'run_01' etc for lo
lo_lhe_dir_regex = '.*run\_[0-9]+?$'
nlo_lhe_dir_regex = '.*run\_[0-9]+?\_decayed\_[0-9]+$'

# match .lhe or .lhe.gz files
LHEF_pattern = '(.*events\.lhe(\.gz)?)$'
#LHEF_pattern = '.*events\.lhe(\.gz)?$'

# match run banner files
banner_pattern = '.*banner.txt$'

# match definitions of muR/muF
fixed_muR_flag_pattern = '\s+(\w+)\s+=\sfixed_ren_scale'
fixed_muF_flag_pattern = '\s+(\w+)\s+=\sfixed_fac_scale'

# match multiplier on scales
fixed_muR_multiplier_pattern = '\s+(\w+)\s+=\smur_over_ref'
fixed_muF_multiplier_pattern = '\s+(\w+)\s+=\smuf_over_ref'

# vals
fixed_muR_val_pattern = '([0-9]+(\.[0-9]+)*)\s+=\s(mur_ref_fixed|scale)'
fixed_muF_val_pattern = '([0-9]+(\.[0-9]+)*)\s+=\s(muf_ref_fixed|dsqrt\_q2fact\w)'

# path to main89 executable for doing umeps/unlops merging
pythia_exec_path = '/home/ucl/cp3/lmoore/MG5_aMC_v2_6_0/examples/main89'

# pathst to preconfigured versions of pythia shower merging commands
cmnd_cards_path = '/home/ucl/cp3/lmoore/topspin/bin/job_submission/test_lhe_source/cmnds/'#MG5_aMC_v2_6_0/examples/cmnds/'
#cmnd_cards_path = '/home/lmoore/Physics/Analyses/baseana/bin/job_submission/test_lhe_source/cmnds'
#cmnd_cards_path = '/home/liamm/Physics/topspin/bin/job_submission/test_lhe_source/cmnds'

cmnd_card_umeps_lo_1j = 'tt_dilept_lo_eft_umeps_1j.cmnd'
cmnd_card_umeps_lo_2j = 'tt_dilept_lo_eft_umeps_2j.cmnd'
cmnd_card_unlops_nlo_1j = 'tt_dilept_nlo_sm_unlops_1j.cmnd'
#cmnd_card_unlops_lo_2j = 'tt_dilept_lo_sm_unlops_2j.cmnd'

# ivy bridge node names
ivy_bridge_nodes = ['mb-ivy211','mb-ivy212','mb-ivy213','mb-ivy214','mb-ivy215','mb-ivy216','mb-ivy217','mb-ivy219','mb-ivy220','mb-ivy226','mb-ivy221','mb-ivy222','mb-ivy223','mb-ivy224','mb-ivy225','mb-ivy227']
# not sure after 220...

#-------------------------------------------------------------------------------------
# functions for extracting paths, manipulating files in LHEF directories, build cmnds
#-------------------------------------------------------------------------------------

#
## get absolute paths to files/directories in directory argument (which optionally match a pattern)
def getAbsolutePaths(directory, include_dirs=True, include_files=True, dir_pattern='.*',file_pattern='.*'):

    # initialise lists to hold directories and files
    matching_dirs=[]
    matching_files=[]

    # accumulate absolute paths of files and directories within target dir
    for dir_path,dir_name,file_name in os.walk(directory):
            if include_dirs == True:
                abs_dir_paths = map( lambda x: os.path.abspath(os.path.join(dir_path,x)), dir_name)
                matching_dirs.extend(abs_dir_paths)
            if include_files == True:
                abs_file_paths = map( lambda x: os.path.abspath(os.path.join(dir_path,x)), file_name)
                matching_files.extend(abs_file_paths)

    # filter down directories and files to those which match patterns if specified
    matching_dirs = filter(lambda x: re.match(dir_pattern,x), matching_dirs)
    matching_files = filter(lambda x: re.match(file_pattern,x), matching_files)

    return matching_dirs + matching_files

##
#

#
## flatten multidimensional lists
def flattenList(l):
    return list(itertools.chain.from_iterable(l))
##
#

#
##  get the LHEF absolute path in a run directory and write this appropriately
### into the shower .cmnd card
def specifyLHEFInCard(cmnd_card_path, LHEF_path):

    # now parse fortran param.inc and match against the extracted couplings
    with open(cmnd_card_path, "r+") as cmnd_card_file:

        # store text_file
        cmnd_text = cmnd_card_file.read()

        # look for LHEF specification
        try:
            print '\nWriting absolute path of LHEF to .cmnd card...\n'

            # replace base LHEF file with full path to that in this file if it exists
            modified_cmnd_text = re.sub('(Beams:LHEF\s+=\s){}'.format(LHEF_pattern),r'\1{}'.format(LHEF_path), cmnd_text)

            # debug
            #print("we have made the substitution now:")
            #print(modified_cmnd_text)
            #print("found line:\n {x}".format(x=re.findall('(Beams:LHEF\s+=\s){}'.format(LHEF_pattern),cmnd_text)))

            # save file with replaced LHE path
            cmnd_card_file.write(modified_cmnd_text)
            #
            #os.path.isfile(fname)

        except AttributeError:

            # no defs of this form in file_text
            print 'No such LHEF path specifiers found in file: {}'.format(cmnd_card_path)

        ##
        #

###
##
#

#
##  get the LHEF absolute path in a run directory and write this appropriately
### into the shower .cmnd card
def specifyCardParams(cmnd_card_path, LHEF_path, banner_path, default_muR, default_muF ):

    # now parse fortran param.inc and match against the extracted couplings
    with open(cmnd_card_path, "r+") as cmnd_card_file:

        # store text_file
        cmnd_text = cmnd_card_file.read()

        # modify cmnd card params

        # absolute path to LHE
        try:
            # look for LHEF specification and ammend it to use the absolute path
            print '\nWriting absolute path of LHEF to .cmnd card...\n'

            # replace base LHEF file with full path to that in this file if it exists
            modified_cmnd_text = re.sub('(Beams:LHEF\s+=\s){}'.format(LHEF_pattern),r'\1{}'.format(LHEF_path), cmnd_text)
            # careful for 2j, seems to be dependent on position of line in file as to whether events.lhe.gz etc matches...

        except AttributeError:

            # no defs of this form in file_text
            print 'No such LHEF path specifiers found in file: {}'.format(cmnd_card_path)

        ##
        #

        # make sure muR/muF match if fixed and warn if not
        try:

            # look inside banner file for muR/muF values and modify cmmd card params to match these accordingly
            print 'Extracting muR/muF information from MG5_aMC banner:\n'
            print banner_path, '\n'
            with open(banner_path, "r") as banner_file:

                banner_text = banner_file.read()

                # one flag/val per file, first group of match extracts boolean + scale in gev respectively
                fixed_muR_flag = re.findall(fixed_muR_flag_pattern, banner_text)[0]
                fixed_muF_flag = re.findall(fixed_muF_flag_pattern, banner_text)[0]
                # these are tuples since 2 groups to capture optional decimal
                fixed_muR_val = re.findall(fixed_muR_val_pattern, banner_text)[0][0]
                fixed_muF_val = re.findall(fixed_muF_val_pattern, banner_text)[0][0]
                # get current scale multiplier parameter
                fixed_muR_multiplier = re.findall(fixed_muR_multiplier_pattern, banner_text)[0][0]
                fixed_muF_multiplier = re.findall(fixed_muF_multiplier_pattern, banner_text)[0][0]

                # Check if fixed muR/muF flags are on and get values
                write_muR = False
                write_muF = False

                if fixed_muR_flag == 'True':
                    print 'Found LHEF generated with fixed muR value * multiplier: {} * {}'.format(fixed_muR_val,fixed_muR_multiplier)
                    # turn on flag to overwrite default .cmnd muR scale with this value
                    write_muR = True
                elif fixed_muR_flag == 'False':
                    print 'Warning: found LHEF generated with dynamical muR. This is not strictly compatible with current merging...'

                if fixed_muF_flag == 'True':
                    print 'Found LHEF generated with fixed muF value * multiplier: {} * {}'.format(fixed_muF_val,fixed_muF_multiplier)
                    # turn on flag to overwrite default .cmnd muF scale with this value
                    write_muF = True
                elif fixed_muF_flag == 'False':
                    print 'Warning: found LHEF generated with dynamical muF. This is not strictly compatible with current merging...'

            # write modified scale values
            if write_muR:
                print 'Fixing muR = {} in {}...'.format(fixed_muR_val, cmnd_card_path)
                modified_cmnd_text = re.sub('(Merging:muRen\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(fixed_muR_val*fixed_muR_multiplier), modified_cmnd_text)
                modified_cmnd_text = re.sub('(Merging:muRenInME\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(fixed_muR_val*fixed_muR_multiplier), modified_cmnd_text)
            else:
                print 'Setting muR to default value {} GeV in {}...'.format(default_muR,cmnd_card_path)
                modified_cmnd_text = re.sub('(Merging:muRen\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(default_muR), modified_cmnd_text)
                modified_cmnd_text = re.sub('(Merging:muRenInME\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(default_muR), modified_cmnd_text)

            if write_muF:
                print 'Fixing muF = {} in {}...'.format(fixed_muF_val, cmnd_card_path)
                modified_cmnd_text = re.sub('(Merging:muFac\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(fixed_muF_val*fixed_muF_multiplier), modified_cmnd_text)
                modified_cmnd_text = re.sub('(Merging:muFacInME\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(fixed_muF_val*fixed_muF_multiplier), modified_cmnd_text)
            else:
                print 'Setting muF to default value {} GeV in {}...'.format(default_muF,cmnd_card_path)
                modified_cmnd_text = re.sub('(Merging:muFac\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(default_muF), modified_cmnd_text)
                modified_cmnd_text = re.sub('(Merging:muFacInME\s+=\s)([0-9]+(\.[0-9]+)*)',r'\1 {}'.format(default_muF), modified_cmnd_text)

            #print modified_cmnd_text

            #

        except AttributeError:

            # no defs of this form in file_text
            print 'No such banner file: {} found...'.format(banner_path)

        ##
        #

        # save file with replaced LHE path and scales if necessary
        cmnd_card_file.seek(0)
        cmnd_card_file.write(modified_cmnd_text)
        cmnd_card_file.truncate()

###
##
#

#
## copy a preconfigured .cmnd pythia shower command card to target directory with
### the absolute path to the LHEF within included
def copyCardToLHEFDir(base_cmnd_card_path, run_directory_path, muR, muF):

    # base cmnd card name from abs path
    base_cmnd_card_name = base_cmnd_card_path.split('/')[-1]

    # base name of target directory
    base_dir_name = run_directory_path.split('/')[-1]

    # look inside run directory containing .lhe or .lhe.gz file + banner file for filename
    for _, _, filenames in os.walk(run_directory_path):
        LHEF = filter(lambda x: re.match(LHEF_pattern, x), filenames)
        banner = filter(lambda x: re.match(banner_pattern, x), filenames)
        if LHEF == []:
            print 'Warning: no file of the format [...]events.lhe(.gz) in {}...'.format(run_directory_path)
        elif banner == []:
            print 'Warning: no banner file found in {}...'.format(run_directory_path)
        elif len(LHEF) >= 2:
            # will need to update this for the +lo 2j unlops case
            print 'Warning: more than one [...]events.lhe(.gz) file found in {}. Assuming first to be showered:\n\n {}\n'.format(LHEF[0])
        elif len(banner) >= 2:
            print 'Warning: more than one banner file found in {}. Reading the first to extract scale information...'.format(banner[0])
        break

    # copy the base version of the cmnd card to the run directory
    print 'Copying {} to {}...'.format(base_cmnd_card_name,run_directory_path)
    full_cmnd_card_path = '{}/{}'.format(run_directory_path,base_cmnd_card_name)
    shutil.copy(base_cmnd_card_path, full_cmnd_card_path )

    # modify this card to reflect the absolute path of the LHEF file within which is to be passed to pythia
    #specifyLHEFInCard(full_cmnd_card_path, '{}/{}'.format(run_directory_path,LHEF[0]))
    specifyCardParams(full_cmnd_card_path, '{}/{}'.format(run_directory_path,LHEF[0]),'{}/{}'.format(run_directory_path,banner[0]),muR,muF)

##
#

#
## given a run dir run_XX[...]/, make an absolute path string for a .yoda file with the same name run_XX[...].yoda
def runYodaPath(run_directory_path):

    # base name of run directory (run_XX...)
    base_dir_name = run_directory_path.split('/')[-1]
    # path to run dir
    run_dir_path = run_directory_path.split('/')[0:-2]

    return '{}/{}.yoda'.format(run_directory_path,base_dir_name)

##
#

#
## given a run dir run_XX[...]/, make an absolute path string for a fifo file with the same name run_XX[...]_fifo.hepmc
def runFIFOPath(run_directory_path):

    # base name of run directory (run_XX...)
    base_dir_name = run_directory_path.split('/')[-1]
    # path to run dir
    run_dir_path = run_directory_path.split('/')[0:-2]

    return '{}/{}_fifo.hepmc'.format(run_directory_path,base_dir_name)

##
#

#
## get absolute path to command card in run directory
### assuming first and only .cmnd in list that matches is what we want
def runCmndPath(run_directory_path):

    # match the .cmnd file in run directory path
    return getAbsolutePaths(run_directory_path,include_dirs=False,include_files=True,file_pattern='.*\.cmnd$')[0]

##
#

#
## get absolute path to command card in run directory
### assuming first and only .cmnd in list that matches is what we want
def runBannerPath(run_directory_path):

    # match the .cmnd file in run directory path
    return getAbsolutePaths(run_directory_path,include_dirs=False,include_files=True,file_pattern=banner_pattern)[0]

##
#

#
## get absolute path to LHEF in run directory
### assuming first and only .lhe(.gz) file in list that matches is what we want
def runLHEFPath(run_directory_path):

    # match the .lhe(.gz) file in run directory path
    return getAbsolutePaths(run_directory_path,include_dirs=False,include_files=True,file_pattern=LHEF_pattern)[0]


##
#

#
## write a sequence of arguments for bash commands to make a FIFO stream, feed pythia events into it with a given cmnd card
### then analyse these events with the given rivet analysis
def runCommands(run_directory_path, executable = pythia_exec_path, ana_name = rivet_analysis_name):

    # header line for each command set
    run_cmnd_header = '# Shower events with pythia program {} and run analysis {}'.format('main89', ana_name)
    # create fifo
    make_fifo_cmd = 'mkfifo {}'.format(runFIFOPath(run_directory_path))
    # run pythia program executable with cmnd card and write output to fifo
    run_pythia_cmd = '{} {} {} &'.format(executable, runCmndPath(run_directory_path), runFIFOPath(run_directory_path))
    # attach rivet analysis to fifo and write out event histograms to yoda file
    run_analysis_cmd = 'rivet -a {} -H {} {}'.format(ana_name, runYodaPath(run_directory_path), runFIFOPath(run_directory_path))
    # write out text file to show when succeeded
    log_successful = "echo -e '# rivet run successfully completed with commands:\\n\\n{}\\n\\n{}\\n\\n{}' >> {}/rivet_log.txt ".format(make_fifo_cmd,run_pythia_cmd,analyse_cmd,run_directory_path)

    #return(make_fifo_cmd,run_pythia_cmd,run_analysis_cmd)
    return ['# # # # #\n',run_cmnd_header,'\n# # # # #\n\n', make_fifo_cmd,'\n\n', run_pythia_cmd,'\n\n', analyse_cmd,'\n\n', log_successful,'\n\n']

###
##
#

#
## write a sequence of arguments for bash commands to make a FIFO stream, feed pythia events into it with a given cmnd card
### then analyse these events with the given rivet analysis
def buildRunCommands(exec_path, ana_name, fifo_path, cmnd_path, yoda_path):

    # header line for each command set
    run_cmnd_header = '# Shower events with pythia program {} and run analysis {}'.format('main89', ana_name)
    # create fifo
    make_fifo_cmd = 'mkfifo {}'.format(fifo_path)
    # run pythia program executable with cmnd card and write output to fifo
    run_pythia_cmd = '{} {} {} &'.format(exec_path, cmnd_path, fifo_path)
    # attach rivet analysis to fifo and write out event histograms to yoda file
    run_analysis_cmd = 'rivet -a {} -H {} {}'.format(ana_name, yoda_path, fifo_path)

    #return(make_fifo_cmd,run_pythia_cmd,run_analysis_cmd)
    return ['# # # # #\n',run_cmnd_header,'\n# # # # #\n\n', make_fifo_cmd,'\n\n', run_pythia_cmd,'\n\n',run_analysis_cmd]

###
##
#
