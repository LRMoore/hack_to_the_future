(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9478,        264]
NotebookOptionsPosition[      9156,        249]
NotebookOutlinePosition[      9516,        265]
CellTagsIndexPosition[      9473,        262]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "Clean", " ", "up", " ", "index", " ", "names", " ", "to", " ", "a", " ", 
    "standard", " ", "form"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "be", " ", "careful", " ", "not", " ", "to", " ", "define", " ", 
     "variables", " ", "with", " ", "these", " ", "index", " ", "names"}], 
    ",", " ", 
    RowBox[{
    "then", " ", "would", " ", "need", " ", "to", " ", "wrap", " ", "in", " ",
      "a", " ", "module", " ", "somewhere", " ", "here", " ", "which", " ", 
     "would", " ", "defeat", " ", "purpose"}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "Otherwise", " ", "can", " ", "do", " ", "something", " ", "like"}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"Clear", "@@", 
    RowBox[{"IndAbbr", "[", "type", "]"}]}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "Define", " ", "some", " ", "abbreviated", " ", "index", " ", "variable", 
    " ", "names"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", 
      RowBox[{"type_", ",", "n_Integer"}], "]"}], ":=", 
     RowBox[{
      RowBox[{"IndAbbr", "[", "type", "]"}], "[", 
      RowBox[{"[", "n", "]"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "Lorentz", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{
      "\[Mu]", ",", "\[Nu]", ",", "\[Rho]", ",", "\[Sigma]", ",", "\[Delta]", 
       ",", "\[Kappa]", ",", "\[Lambda]", ",", "\[Alpha]", ",", "\[Beta]"}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "Spin", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{
      "s1", ",", "s2", ",", "s3", ",", "s4", ",", "s5", ",", "s6", ",", "s7", 
       ",", "s8"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "Generation", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{"p", ",", "r", ",", "s", ",", "t", ",", "u", ",", "v"}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "SU2D", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{"i", ",", "j", ",", "k", ",", "l", ",", "m", ",", "n"}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "Colour", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{
      "a", ",", "b", ",", "c", ",", "d", ",", "e", ",", "f", ",", "g", ",", 
       "h"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "Gluon", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{
      "AA", ",", "BB", ",", "CC", ",", "DD", ",", "EE", ",", "FF", ",", "GG", 
       ",", "HH"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"IndAbbr", "[", "SU2W", "]"}], ":=", 
     RowBox[{"{", 
      RowBox[{
      "II", ",", "JJ", ",", "KK", ",", "MM", ",", "NN", ",", "OO", ",", 
       "PP"}], "}"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"Given", " ", "a", " ", "list", " ", "of", " ", "indices"}], ",",
      " ", 
     RowBox[{
     "generate", " ", "rules", " ", "to", " ", "rename", " ", "these", " ", 
      "in", " ", "order"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"$StartInd", ":=", "1"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"MakeRenameIndexRules", "[", 
      RowBox[{"arglist", ":", 
       RowBox[{"{", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Index", "[", 
           RowBox[{"type_", ",", "_"}], "]"}], ".."}], ")"}], "}"}]}], "]"}], 
     ":=", 
     RowBox[{"Block", "[", 
      RowBox[{
       RowBox[{"{", "newarglist", "}"}], ",", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"newarglist", "=", 
         RowBox[{"DeleteCases", "[", 
          RowBox[{"arglist", ",", " ", 
           RowBox[{"Index", "[", 
            RowBox[{"_", ",", "_Ext"}], "]"}]}], "]"}]}], ";", 
        "\[IndentingNewLine]", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"MapThread", "[", 
          RowBox[{"Rule", ",", 
           RowBox[{"{", 
            RowBox[{"newarglist", ",", 
             RowBox[{"ToExpression", "/@", 
              RowBox[{"(", "\[IndentingNewLine]", 
               RowBox[{
                RowBox[{
                 RowBox[{"ToString", "[", 
                  RowBox[{"Index", "[", 
                   RowBox[{"type", ",", 
                    RowBox[{"IndAbbr", "[", 
                    RowBox[{"type", ",", "#"}], "]"}]}], "]"}], "]"}], "&"}], 
                "/@", 
                RowBox[{"(", 
                 RowBox[{"Range", "[", 
                  RowBox[{"Length", "[", "newarglist", "]"}], "]"}], 
                 "\[IndentingNewLine]", ")"}]}], ")"}]}]}], "}"}]}], "]"}], "//",
          "Flatten"}]}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
      "]"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"MakeRenameIndexRules", "[", 
      RowBox[{"arglists", ":", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"_Index", ".."}], "}"}], ".."}], "}"}]}], "]"}], ":=", 
     RowBox[{
      RowBox[{"MakeRenameIndexRules", "/@", "arglists"}], "//", "Flatten"}]}],
     ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"Optimize", " ", "inactive", " ", "expressions"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Clear", "[", "myOptimizeIndex", "]"}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"myOptimizeIndex", "[", "expr_", "]"}], ":=", "expr"}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"myOptimizeIndex", "[", "expr_List", "]"}], ":=", 
     RowBox[{"myOptimizeIndex", "/@", "expr"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"myOptimizeIndex", "[", 
      RowBox[{"expr__", "?", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"FreeQ", "[", 
          RowBox[{"#", ",", "_Index"}], "]"}], "&"}], ")"}]}], "]"}], ":=", 
     "expr"}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"myOptimizeIndex", "[", "expr1_Plus", "]"}], ":=", 
    RowBox[{"Plus", "@@", 
     RowBox[{"(", 
      RowBox[{"myOptimizeIndex", "/@", 
       RowBox[{"(", 
        RowBox[{"List", "@@", "expr1"}], ")"}]}], ")"}]}]}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"myOptimizeIndex", "[", 
     RowBox[{"expr_", "?", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"!", 
          RowBox[{"MatchQ", "[", 
           RowBox[{
            RowBox[{"Head", "@", "#"}], ",", "Plus"}], "]"}]}], "&&", 
         RowBox[{"!", 
          RowBox[{"FreeQ", "[", 
           RowBox[{"#", ",", "_Index"}], "]"}]}]}], "&"}], ")"}]}], "]"}], ":=", 
    RowBox[{"Block", "[", 
     RowBox[{
      RowBox[{"{", "sortInds", "}"}], ",", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
       "Form", " ", "organized", " ", "list", " ", "of", " ", "indices", " ", 
        "in", " ", "operator", " ", "by", " ", "type"}], " ", "*)"}], 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"sortInds", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"Cases", "[", 
           RowBox[{"expr", ",", "_Index", ",", "Infinity"}], "]"}], "//", 
          "Union"}], "//", 
         RowBox[{
          RowBox[{"GatherBy", "[", 
           RowBox[{"#", ",", 
            RowBox[{
             RowBox[{"#", "/.", 
              RowBox[{
               RowBox[{"Index", "[", 
                RowBox[{"type_", ",", "_"}], "]"}], ":>", "type"}]}], "&"}]}],
            "]"}], "&"}]}]}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"(*", " ", 
        RowBox[{"Generate", " ", "rename", " ", "rules"}], " ", "*)"}], 
       "\[IndentingNewLine]", 
       RowBox[{"expr", "/.", 
        RowBox[{"MakeRenameIndexRules", "[", "sortInds", "]"}]}]}]}], 
     "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.708677490972538*^9, 3.708677597409573*^9}, 
   3.708928118683771*^9},ExpressionUUID->"686b495b-39ed-4777-9787-\
61a70c89127b"]
},
WindowSize->{1918, 1020},
WindowMargins->{{Automatic, 1483}, {Automatic, -18}},
FrontEndVersion->"11.1 for Mac OS X x86 (32-bit, 64-bit Kernel) (April 18, \
2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 8594, 227, 1062, "Input", "ExpressionUUID" -> \
"686b495b-39ed-4777-9787-61a70c89127b"]
}
]
*)

