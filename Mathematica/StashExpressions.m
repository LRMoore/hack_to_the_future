(* ::Package:: *)

(* ::Title:: *)
(*Basic file-backed expression stashing/loading*)


(* ::Input::Initialization:: *)
(* Expression stashing v1.0 *)

(* by Liam Moore *)


(* ::Section:: *)
(*Functions and definitions*)


(* ::Subsection:: *)
(*Paths and helper functions*)


(* ::Subsubsection:: *)
(*Set and test contents of stash directory path*)


(* ::Input::Initialization:: *)
(* define directory path variable for storing large expressions that we don't want to recompute every time *)
(*$allYourBasesStashPath=$AllYourBasesPath<>"/AllYourBases_Core/Stored_Identities/";*)
(*$stashPath=$allYourBasesStashPath;*)
$stashPath=NotebookDirectory[];

(* keep track of all files in stash *)
$stashContents:=FileNames[RegularExpression[".*"],$stashPath];

(* check if .mx file for a particular symbol exists in stash *)
ClearAll[inStashQ];
SetAttributes[inStashQ,HoldFirst];
inStashQ::wronghead="Should take a symbol as an argument";
inStashQ[Except[_Symbol]]:="nothing"/;Message[inStashQ::wronghead];
inStashQ[expr_Symbol]:=!FreeQ[$stashContents,($stashPath<>SymbolName[Unevaluated[expr]]<>".mx")];


(* ::Subsubsection::Closed:: *)
(*Assign filenames to variables, look up their definitions*)


(* ::Input::Initialization:: *)
(* collect together all the defining properties of a given symbol/function *)
allDefinitions= 	{		
		DownValues,
		UpValues,
		OwnValues,
		SubValues,
		FormatValues,
		NValues,
		Options,
		DefaultValues
	};

(* subset of these that handle actual assignments *)
assignmentDefinitions={DownValues,UpValues,OwnValues,NValues,SubValues,DefaultValues};


(* ::Input::Initialization:: *)
(* define a stash path string for a given symbol *)
ClearAll[stashPathString];
SetAttributes[stashPathString,HoldFirst];
stashPathString::wronghead=inStashQ::wronghead;
stashPathString[Except[_Symbol]]:="nothing"/;Message[stashPathString::wronghead];
stashPathString[expr_Symbol]:=($stashPath<>SymbolName[Unevaluated[expr]]<>".mx")

(* Check for existing symbol definitions *)
ClearAll[hasDefinitions];
SetAttributes[hasDefinitions,HoldFirst];
hasDefinitions[expr_Symbol]:=Flatten@(#[expr]&/@assignmentDefinitions)//!MatchQ[#,{}]&;

(* Get definitions *)
ClearAll[getDefinitions];
SetAttributes[getDefinitions,HoldAllComplete];
getDefinitions[expr_Symbol]:=Block[{makePropertyList},

(* Print output *)
Print["Looking up definitions for "<>SymbolName[Unevaluated[expr]]<>"..."];

(* helper function to build list of form e.g. {UpValues, UpValues[argument_]} *)
makePropertyList[prop_,ex_]:={prop,prop[ex]}/;!MatchQ[prop[ex],{}];
(* return nothing if no e.g. UpValues, DownValues etc defined *)
makePropertyList[prop_,ex_]:=Sequence[]/;MatchQ[prop[ex],{}];

(* Print lists of the Up, Down, OwnValues... etc associated with the symbol expr *)
makePropertyList[#,expr]&/@assignmentDefinitions

];

(* work on lists too *)
getDefinitions[exprList:{_Symbol..}]:=getDefinitions/@exprList;


(* ::Subsection:: *)
(*Main functions for storing and loading*)


(* ::Subsubsection::Closed:: *)
(*storeDefinitions*)


(* ::Input::Initialization:: *)
(* write out the argument of this function to the stash *)
ClearAll[storeDefinitions];
SetAttributes[storeDefinitions,HoldFirst];
Options[storeDefinitions]={overwriteStash->False};
storeDefinitions::nostash="<$AllYourBasesPath>/AllYourBases_Core/Stored_Identities/ directory missing. Creating it...";
storeDefinitions::nooverwrite="Associated .mx file for this symbol already exists. To overwrite, specify the overwriteStash->True option here...";
storeDefinitions::nodefs="Nothing defined for this symbol.";
storeDefinitions::wronghead="Should take a symbol as an argument";


(* main usage *)
storeDefinitions[expr_Symbol,opt:OptionsPattern[]]:=Block[{},

(* check for overwrite *)
If[OptionValue[overwriteStash],##[]&,
If[inStashQ[expr],Message[storeDefinitions::nooverwrite];Return[],##[]&]
];

(* check there are any existing definitions for expr  to save*)
If[!hasDefinitions[expr],Message[storeDefinitions::nodefs];Return[]];

(* check for  existence of stash directory *)
If[DirectoryQ[$stashPath],

(* write out symbol and all definitions to file if there, with the file title "expr.mx" *)
Print["Writing definitions for "<>SymbolName[Unevaluated[expr]]<>" to file..."];
DumpSave[stashPathString[expr],expr],

(* Show warning then create directory first if not *)
Message[storeDefinitions::nostash];CreateDirectory[$stashPath];
DumpSave[stashPathString[expr],expr]

];


];

(* work also on lists of symbols *)
storeDefinitions[exprList:{_Symbol..},opt:OptionsPattern[]]:=Scan[storeDefinitions[#,opt]&,exprList];

(* handle case where file already exists *)
storeDefinitions[expr_Symbol]/;inStashQ[expr]:=Message[storeDefinitions::nooverwrite];

(* work only on symbols *)
storeDefinitions[expr:Except[_Symbol]]:="nothing"/;Message[storeDefinitions::wronghead];


(* ::Subsubsection::Closed:: *)
(*loadDefinitions*)


(* ::Input::Initialization:: *)
(* check stash for file associated with symbol and load if it exists *)
ClearAll[loadDefinitions];
SetAttributes[loadDefinitions,HoldFirst];
Options[loadDefinitions]={overwriteDefinitions->False};
loadDefinitions::nostash="<$AllYourBasesPath>/AllYourBases_Core/Stored_Identities/ directory missing. Can't load definitions.";
loadDefinitions::nofile="No corresponding .mx file found in stash.";
loadDefinitions::alreadydef="This symbol is already defined. To overwrite current definitions with those from file, specify overwriteDefinitions->True here...";
loadDefinitions::wronghead=storeDefinitions::wronghead;

(* main usage *)
loadDefinitions[expr_Symbol,opt:OptionsPattern[]]:=Block[{},

(* check for overwrite *)
If[OptionValue[overwriteDefinitions],##[]&,
(* print a warning if there are already definitions associated with this symbol *)
If[hasDefinitions[expr],Message[loadDefinitions::alreadydef];Return[],##[]&];
];

(* check for stash directory *)
If[DirectoryQ[$stashPath],

(* look for corresponding .mx file for expr *)
If[inStashQ[expr],Print["Loading "<>SymbolName[Unevaluated[expr]]<>" from file..."];Evaluate[Get[stashPathString[expr]]],"nothing"/;Message[loadDefinitions::nofile]],

(* Show warning if not *)
Message[storeDefinitions::nostash]

]

];

(* work also on lists of symbols *)
loadDefinitions[exprList:{_Symbol..},opt:OptionsPattern[]]:=Scan[loadDefinitions[#,opt]&,exprList];

(* work only on symbols *)
loadDefinitions[expr:Except[_Symbol]]:="nothing"/;Message[loadDefinitions::wronghead];


(* ::Subsubsection::Closed:: *)
(*TODO: add flexible identifier (Date, package version etc) to aid comparison of definitions for the same symbol which differ*)


(* ::Input::Initialization:: *)
(**)


(* ::Subsubsection::Closed:: *)
(*TODO: add compareDefinitions(File<->Notebook, use Complement?)*)


(* ::Input::Initialization:: *)
(**)
