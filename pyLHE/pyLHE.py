#!/usr/bin/env python
from collections import Iterator, MutableSequence
import StringIO 
import os
import re
import sys
import xml.etree.ElementTree as ET
from itertools import izip
################################################################################
# Structs for LHE init info, event info and particles
class Info(object):
    '''
    Generic Les Houches Event info template class. Expects data members named 
    according to names and typed according to casts. Accepts comment keyword 
    argument and prints the contents after the line information. Printing can  
    be sandwiched in between <tag>, </tag> if tag is defined.
    '''
    names = []
    casts = []
    tag = None
    def __init__(self, line, comments=[]):
        self.comments = comments

        # set opening and closing enviroment tags
        if self.tag is not None:
            self.op = '<{}>\n'.format(self.tag)
            self.cl = '\n</{}>\n'.format(self.tag)
        else:
            self.op, self.cl = '', ''
        
        # list of values from line input
        vals = line.split() 
        
        # cast all elements to string if casts not defined
        if not self.casts:
            self.casts = [str for _ in  self.names]
        
        # store variables in self.__dict__ so they can be accessed as data
        # members
        for name, cast, val in zip(self.names, self.casts, vals):
            self.__dict__[name] = cast(val)
            
    def __str__(self):
        # list of values
        data = [self.__dict__[n] for n in self.names]
        # format string, floats will be printed in scientific notation to 9
        # decimal places
        fstr = '\t'.join([r'{:.9E}' if c is float else r'{}' 
                          for c in self.casts])
        info = (fstr).format(*data)
        
        return self.op + '\n'.join(self.comments+[info]) + self.cl


class LHEinit(Info):
    '''Les Houches Event file init information'''
    names = ['IDBM1', 'IDBM2', 'EBM1', 'EBM2', 'PDFG1', 'PDFG2', 
             'PDFS1', 'PDFS2', 'IDWT', 'NPR', 
             'XSEC', 'XERR', 'XMAX', 'LPR']
    casts = [int,   int,   float, float, int,   int, 
             int,   int,   int,   int,   float, 
             float, float, int,   int]
    tag = 'init'
    def __str__(self):
        data = [self.__dict__[n] for n in self.names]
        info1 = ('\t{}'*10).format(*data[:10])
        info2 = ('\t{}'*4).format(*data[10:])
        info = [info1,info2]
        return self.op + '\n'.join(self.comments+info) + self.cl
        
class LHEinfo(Info):
    '''Les Houches Event init information'''
    names = ['N', 'IDPR', 'XWGT', 'SCAL', 'AQED', 'AQCD']
    casts = [int,  int,   float,  float,  float,  float]

class LHEparticle(Info):
    '''Les Houches Event  particle information'''
    names = ['ID', 'IST', 'MOTH1', 'MOTH2', 'ICOL1', 'ICOL2', 
             'P1', 'P2', 'P3', 'P4', 'P5', 'VTIM', 'SPIN'] 
    casts = [int for _ in range(6)] + [float for _ in range(7)]
    
################################################################################
# Event class
class LHE(MutableSequence):
    '''LHE class. Behaves as an indexable sequence of particles.'''
    
    def __init__(self, info, particles, extras={}, comments=[]):
        self.info = info
        self.particles = particles
        self.comments = comments
        try:
            self._rwgt = extras.pop('rwgt')
            self._rwgt.tail = self._rwgt.tail.strip()+'\n'
        except KeyError:
            self._rwgt = None
        self.extras = extras
        
    def __getitem__(self, index):
        return self.particles[index]
    
    def __setitem__(self, index, value):
        self.particles[index] = value
    
    def __delitem__(self, index):
        del self.particles[index]
        
    def __len__(self):
        return len(self.particles)
    
    def insert(self, index, value):
        self.particles = sum(self.particles[:index],
                            [value],
                            self.particles[index:])
    
    def __str__(self):
        return ET.tostring(self.xml())

        #rwgt =  '' if self._rwgt is None else ET.tostring(self._rwgt )
        #comment = '\n'.join(self.comments)
        #fstr = '<event>\n\t{}\n\t{}\n{}\n{}\n</event>\n'.format 
        #return fstr(self.info, parts, rwgt, comment)

    def xml(self):
        particles = '\n\t' + '\n\t'.join([str(x) for x in self.particles]) + '\n'
        
        evt = ET.Element('event')
        evt.text = '\n\t{}{}'.format(self.info, particles)
        if self._rwgt is not None: 
            evt.append(self._rwgt)
        evt.tail = '\n'.join(self.comments) +'\n'
        
        return evt
    
    # utility functions
    # def dwgt(self, index=0):
    
    def wgt(self, index=0):
        if self._rwgt is not None:
            return float(self._rwgt[index].text)
        else:
            return self.info.XWGT
    
    def wgts(self):
        if self._rwgt is not None:
            return [float(x.text) for x in self._rwgt]
        else:
            return [self.info.XWGT]
    
    def wgts_dict(self):
        if self._rwgt is not None:
            return { self._rwgt[i].attrib['id']:float(self._rwgt[i].text) 
                     for i in range(len(self._rwgt))}
        else:
            return None
    
    def get_pid(self, *pids):
        '''Get list of particles with PIDs given by arguments.'''
        return [p for p in self if p.ID in pids]
    
    def remove_pid(self, *pids):
        '''
        Remove all external particles with PIDs given as arguments & modify 
        self.info.N and particle mother indices accordingly. 
        Mothers of removed particles will have their status changed to 1.
        '''
        to_remove = [x for x in self.get_pid(*pids) if x.IST==1]
        removed = 0
        for p in to_remove:
            # index of particle to remove
            ind = self.index(p)+1
            # fix mother indices of other particles & status of p's parent
            for i, o in enumerate(self):
                if i in (p.MOTH1, p.MOTH2): o.IST = 1
                if o.MOTH1 > ind: o.MOTH1 -= 1
                if o.MOTH2 > ind: o.MOTH2 -= 1                

            self.remove(p)    
            removed +=1
            
        self.info.N -= removed


################################################################################
# Main iterator class
class LHEfile(Iterator):
    '''
    Iterator for LHE file. Stores preamble and initialisation info and has 
    an iterator for the open file. The next() method returns the next LHE in 
    the file. nevents option specifies that only the first nevent events are 
    read before raising StopIteration.
    '''
    def __init__(self, file, nevents = None):
        # file handle
        self.handle = open(file, 'r')
        self.name = file
        # counters
        self.nevents = nevents
        self.nread = 0
        # file handle iterator
        self.iter = iter(self.handle)
        self.line=''
        
        # go to first line of file
        self.__nextline__()
        self.preamble = StringIO.StringIO()
        # Any duplicate files instantiated by clone() method
        self.clones = []
        self.nrecords = []
        
        # read header
        prelines = []
        while not self.line.startswith('<init>'):
            pline = self.line.strip()
            print >> self.preamble, pline

            if pline:
                prelines.append(pline)
            self.__nextline__()
        
        with open('header.lhe', 'w') as hdr:
            hdr.write('\n'.join(prelines[1:]))
        # parse header
        try:
            header = ET.fromstring('\n'.join(prelines[1:]))
        except ET.ParseError as e:
            print 'Error reading header: line {}, column {}.'.format(e.position[0]+1,e.position[1])
            print 'Problematic line:'
            print repr(prelines[e.position[0]])
            print 'Problematic character:'
            print repr(prelines[e.position[0]][e.position[1]])
            raise StopIteration
        # header = ET.fromstring('\n'.join(prelines))
        
        # store rwgt initialisation info
        self.initrwgt = header.find('initrwgt')
        if ((self.initrwgt is not None) and (len(self.initrwgt)==0)):
            # <![CDATA[ ... ]]> thing
            self.initrwgt = ET.fromstring('<initrwgt>'+self.initrwgt.text+'</initrwgt>')

        # read <init> info
        lines, comments = self.read_until('</init>')

        # ignore the </init> line and collect all other fields into one line
        self.init = LHEinit(' '.join(lines))
        
        # set self.line to first occurence of <event>
        while not self.line.strip().startswith('<event>'):
            self.__nextline__()
    
    # Reading functions
    @staticmethod
    def _parse_nextline(line):
        # process string to escape "<" character 
        # not at the beginning of the line
        line = re.sub(r'(?<!^)<(?!/)','&lt;',line)
        # tabs to 4 spaces
        line = re.sub(r'\x09+', '    ', line)
        # remove ASCII control characters!?
        line = re.sub(r'[\x00-\x1F]+', '', line)
        return line.strip()
        
    def __nextline__(self):
        '''
        Set current line of LHE file to next line of lhe file.
        '''
        try:
            # ignore whitespace
            self.line = self._parse_nextline(self.iter.next())
            while self.line == '':
                self.line =  self._parse_nextline(self.iter.next().strip())
                if self.line:
                    break
        except StopIteration as e:
            print 'Stopped iteration over {}.'.format(self.name)
            print 'Last line: ', self.line
            self.close()
            raise StopIteration
    
    def read_until(self, string):
        '''
        Store result of __nextline__() until line beginning with argument 
        'string' is reached. Lines and comments (beginning with '#')are 
        returned in two separate lists. 
        '''
        lines, comments = [], []
        self.__nextline__()
        
        while not self.line.startswith(string):

            if self.line.startswith('#'):
                comments.append(self.line)
            elif self.line.strip()!='':
                lines.append(self.line)
            
            self.__nextline__()
        
        return lines, comments
    
    def next(self):
        '''
        next method for Iterator ABC to give us __iter__() for free. 
        Return LHEevent instance corresponding to next event in LHE file.
        '''
        if self.nread == self.nevents:
            raise StopIteration
            
        # consume lines until beginning of next 'event' tag
        while not self.line.startswith('<event>'):
            self.__nextline__()        
            
        # consume lines until end of 'event' tag
        lines, comments = self.read_until('</event>')

        event = ET.fromstring('\n'.join(['<event>']+lines+['</event>']))
        lines = [l for l in event.text.split('\n') 
                 if l and not l.startswith('#')]
                 
        eventinfo = LHEinfo(lines[0])
        particles = [LHEparticle(l) for l in lines[1:]]
        
        # rwgt = event.find('rwgt')
        # consume lines until beginning of next 'event' tag
        # while not self.line.startswith('<event>'):
        #     self.__nextline__()
            
        self.nread+=1
        
        return LHE(eventinfo, particles, 
                   extras={ e.tag: e for e in event }, 
                   comments=comments)
    
    # Writing functions
    def clone(self, path, keep_header=True):
        '''
        Open a new file handle containing the preamble and init info of 
        current instance. Returns the handle.
        '''
        if not keep_header:
            preamble = StringIO.StringIO()
            print >> preamble, '<LesHouchesEvents>'        
            print >> preamble, '<header>'    
            print >> preamble, '</header>'
        else:
            preamble = self.preamble
            
        self.clones.append(open(path, 'w'))
        self.clones[-1].write(preamble.getvalue())
        self.clones[-1].write(str(self.init))
        self.nrecords.append(0)
        return self.clones[-1]
    
    def record(self, event, index=-1):
        '''
        Write LHE event to self.clone at index, last one by default.
        '''
        self.clones[index].write(str(event))
        self.nrecords[index] += 1
    
    # Getters
    
    def wgt_info(self, index=0):
        if self.initrwgt is not None:
            return self.initrwgt[0][index].text
        else:
            return None
    
    def wgts_info(self):
        if self.initrwgt is not None:
            return [x.text for x in self.initrwgt[0]]
        else:
            return [None]
            
    def wgt_id(self, index=0):
        if self.initrwgt is not None:
            return self.initrwgt[0][index].attrib['id']
        else:
            return None
            
    def wgts_id(self):
        if self.initrwgt is not None:
            return [x.attrib['id'] for x in self.initrwgt[0]]
        else:
            return [None]
            
    def weighting_strategy(self):
        return self.init.IDWT
    
    # Cleanup
    def close(self):
        '''
        Cleanup method for file handles.
        '''
        try:
            for i, cln in enumerate(self.clones):
                cln.write('# nevents = {}\n'.format(self.nrecords[i]))
                cln.write('</LesHouchesEvents>\n')
                if not cln.closed: 
                    # print 'closing {}'.format(cln.name)
                    cln.close()
        except ValueError:
            pass
        try:
            if not self.handle.closed: 
                self.handle.close()
                # print 'closing {}'.format(self.handle.name)
        except (AttributeError, ValueError):
            pass
        
    def __del__(self):
        self.close()



if __name__=='__main__':
    pass
    # os.chdir('/Users/Ken/Work/Projects/NLO/HEL')
    # infile, outfile =  sys.argv[1:]
    # # infile, outfile =  'ZH_LHC13_LO_SM_CT6_0.5mu.lhe', 'clone.lhe'
    #
    # reweight_all('events.lhe','test.reco.lhe')
        # print j
        # print revt
    #     event.remove_pid(5,-5)
    #     higgs = event.get_pid(25)[0]
    #     if higgs.P5 > 120.:
    #         myfile.record(event)
    #         ctr += 1
        # if ctr==10:break
    
    # with open(outfile, 'w') as new:
    #     new.write(myfile.preamble.getvalue())
    #     new.write(str(myfile.init))
    #     ctr = 0
    #     for event in LHEfile(infile):
    #         ctr+=1
    #
    #         event.remove_pid(5,-5)
    #         higgs = event.get_pid(25)[0]
    #         if higgs.P5 > 120.:
    #             new.write(str(event))
    #
    #     new.write('</LesHouchesEvents>')

        
    
    
