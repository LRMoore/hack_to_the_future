#!/usr/bin/env python
import sys
from pyLHE import LHEfile
assert len(sys.argv)==4, 'usage: LHEsubset.py LHEFILE OUTFILE NEVENTS'

inputfile = LHEfile(sys.argv[1])

inputfile.clone(sys.argv[2])

nevts = int(sys.argv[3])

for i, evt in enumerate(inputfile):
    if i==nevts: break
    inputfile.record(evt)
    # if i % int(float(nevts)/10.) ==0 : print i

