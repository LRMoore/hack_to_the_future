#!/usr/bin/env python
import sys
from pyLHE import LHEfile
assert len(sys.argv)==3, 'usage: {} LHEFILE OUTFILE'.format(sys.argv[0])

inputfile = LHEfile(sys.argv[1])

inputfile.clone(sys.argv[2], keep_header=False)

for i, evt in enumerate(inputfile):
    inputfile.record(evt)
    # if i % int(float(nevts)/10.) ==0 : print i

