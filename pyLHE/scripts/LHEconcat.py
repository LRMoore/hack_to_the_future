#!/usr/bin/env python
import sys
from pyLHE import LHEfile
from pyLHE.functions import concatenate
import argparse

parser = argparse.ArgumentParser(description='Concatenate list of LHE event files')
parser.add_argument('outfile', type=str, help='name of output files')
parser.add_argument('infiles', type=str, nargs='+', help='names of input LHE files')

args = parser.parse_args()

concatenate(args.outfile, *args.infiles)



