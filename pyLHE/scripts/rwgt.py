#!/usr/bin/env python
from pyLHE import reweight_from_parton
from sys import argv

assert len(sys.argv)==3, 'usage: rwgt.py PARTONFILE RECOFILE'

partonfile, recofile = argv[1:]

reweight_from_parton(partonfile, recofile)
