#!/usr/bin/env python
import os
import sys
from pyLHE import LHEfile
import xml.etree.ElementTree as ET
import StringIO
from itertools import product, izip
from operator import mul

def weight_to_coeff_default(wgt_dict):
    p0, plus, minus = wgt_dict['rwgt_1'], wgt_dict['rwgt_2'], wgt_dict['rwgt_3']
    
    p1  = (plus - minus)/(2.*cval_sim)
    
    p2 = ((plus + minus) - 2.*p0)/(2.*cval_sim**2)
    
    return {'p0':p0, 'p1':p1, 'p2':p2}

def make_polyweight(partonfile, dimension=1, weight_func=weight_to_coeff_default, 
                    recofile=None, pwfilename=None, nevents=None, keep_header=True):
    '''
    Generate a polyweight formatted .lhe file from a reweighted .lhe sample.
    It stores the coefficients of a quadratic polynomial in some parameters of 
    arbitrary dimension in the form of weights named pijk... 
    i,j,k,...,m denote the power of the 1st, 2nd, 3rd, ..., nth parameter and
    i+j+k+...+n <=2.
    The weight for a given point in parameter space (c1,c2,c3,...,cn) is:
    
        SUM[ pijk...m * c1^i*c2^j*c3^k...cn^m ]
    
    Arguments:
        partonfile  - LHE formatted event file with multiple weights obtained 
                      from reweighting.
    Options:
        dimension   - integer dimension of the parameter space (n).
        weight_func - user-defined function to map the wgts_dict() property of 
                      a pyLHE record to the set of coefficients. A further key 
                      'rwgt_0' is added to the wgts_dict() value before passing 
                      to weight_func. It must return a dictionary with keys 
                      formatted as 'pijk...n', e.g., p012 or p0101.
        recofile    - Specify a reco-level file generated from partonfile, from 
                      which the polyweight file will be generated instead of 
                      partonfile.
        pwfilename  - output file name (default =  partonfile/recofile + '.pw')
        nevents     - number of events to use from partonfile (default = all)
        keep_header - Whether to retain the header information from the source 
                      .lhe file in the outut .pw file (default = True)
    '''
    
    # handle recofile option
    if recofile is not None:
        file_to_clone = LHEfile(recofile, nevents=nevents)
        weight_file = LHEfile(partonfile, nevents=nevents)
    else:
        file_to_clone = LHEfile(partonfile, nevents=nevents)
        weight_file = LHEfile(partonfile, nevents=nevents)
            
    # handle output file option
    if pwfilename is None:
        pwfilename = file_to_clone.name +'.pw'
    
    # create new <initrwgt> element
    newinitrwgt = ET.Element('initrwgt')
    newinitrwgt.text='\n'
    newinitrwgt.tail='\n'
    
    newwgtgrp = ET.SubElement(newinitrwgt, 'polyweights')
    newwgtgrp.text='\n'
    newwgtgrp.text+=str(dimension)+'-dimensional quadratic polynomial coefficients.\n'
    newwgtgrp.text+='Defined as: SUM( pijk...m * c1^i*c2^j*c3^k...cn^m )\n'
    newwgtgrp.tail='\n'

    # create new weights according to dimension
    p = {}
    for ind in product((0,1,2), repeat=dimension):
        # ignore higher than quadratic powers
        if sum(ind)>2: continue
        pname = 'p'+''.join([str(i) for i in ind])
        p[ind] = ET.SubElement(newwgtgrp, 'weight', attrib={'id':pname})
        
        coeffs = ['c{}^{}'.format(i+1,j) if j > 0 else '' for i,j in enumerate(ind)]
        _term = '*'.join( [c for c in coeffs if c is not ''] ).replace('^1','')
        term = _term if _term  else 'Constant'
        p[ind].text=' Coefficient {} for {} term\n'.format(pname, term)
        p[ind].tail='\n'
    
    file_to_clone.initrwgt = newinitrwgt
    
    # Add new <initrwgt> element to header' won't work is there is no header.
    if not keep_header or not file_to_clone.preamble.getvalue().strip():
        # reset preamble
        file_to_clone.preamble = StringIO.StringIO()
        print >> file_to_clone.preamble, '<LesHouchesEvents>'        
        print >> file_to_clone.preamble, '<header>'    
        print >> file_to_clone.preamble, ET.tostring(newinitrwgt)    
        print >> file_to_clone.preamble, '</header>'
    else:
        # modify preamble in place
        newpreamble = StringIO.StringIO()
        preamble_iter =  StringIO.StringIO(file_to_clone.preamble.getvalue())
        # for line in parton.preamble:
        found_initrwgt = False
        for line in preamble_iter:
            if line.strip().startswith('<initrwgt>'):
                found_header=True
                while line.strip() != '</initrwgt>':
                    line = preamble_iter.next()
                print >> newpreamble, ET.tostring(newinitrwgt)
            elif line.strip().startswith('</header>') and not found_initrwgt:
                print >> newpreamble, ET.tostring(newinitrwgt)
                print >> newpreamble, '</header>'
            else:
                print >> newpreamble, line.strip()

        file_to_clone.preamble = newpreamble
    
    poly = file_to_clone.clone(pwfilename)
    
    for j, (wevt, pevt) in enumerate(izip(weight_file, file_to_clone)):

        wgts = wevt.wgts_dict()
        wgts['rwgt_0'] = wevt.info.XWGT
                
        ps = weight_func(wgts)
        
        newrwgt = ET.Element('rwgt')
        newrwgt.text='\n'
        newrwgt.tail='\n'
        
        for k, v in ps.iteritems():
            wgt = ET.SubElement(newrwgt, 'wgt', attrib={'id':k})
            wgt.text = '{:.8e}'.format(v)
            wgt.tail = '\n'
        
        pevt._rwgt = newrwgt
        
        pevt.info.XWGT = 1.
        file_to_clone.record(pevt)
        
        if (j % 10000 == 0): print 'Processed {} events'.format(j)    
        # if (nevents is not None) and (j==nevents):
        #     break
        
    print 'Generated polyweight .lhe file: {}'.format(pwfilename)

def coeff_to_weight_default(cvals, pw_dict):
    '''
    Default function to convert polyweight coefficients into an event weight
    from a point in parameter space. Assumes a general quadratic function of the 
    parameters given in cval = [c1, c2,..., cn] as defined in make_polyweight. 
    The polyweight coefficients are passed as a dictionary {pijk... : vijk...}
    They specify the coefficients of a quadratic polynomial in some parameters of 
    arbitrary dimension in the form of weights named pijk... 
    i,j,k,...,m denote the power of the 1st, 2nd, 3rd, ..., nth parameter and
    i+j+k+...+n <=2.
    The weight for a given point in parameter space (c1,c2,c3,...,cn) is:
    
        SUM[ pijk...m * c1^i*c2^j*c3^k...cn^m ]
    
    Arguments:
        vals    - list of parameter values [c1,c2,...,cn]. Single float value 
                  will be treated specially.
        pw_dict - dict of pijk coefficients
    '''
    try:
        dimension = len(cvals)
    except TypeError:
        cvals = [cvals]
        dimension = 1
    
    wgt=0.
    for k,v in pw_dict.iteritems():
        try:
            exponents = [int(i) for i in k[1:]]
        except ValueError as e:
            print e
            raise AssertionError, 'Incorrect polyweight id format: {}'.format(k)

        assert len(exponents)==len(cvals), 'Polyweight format ({}) does not match length of cvals ({})'.format(len(exponents),len(cvals))

        coeffs = ([ c**e for c,e in zip(cvals, exponents) ])
        
        wgt += v*reduce(mul, coeffs)
        
    return wgt

def gen_polyweight(pwfile, lhefile, cvals, weight_func=coeff_to_weight_default, nevents=None, keep_header=True):
    '''
    Take a polyweight formatted .lhe file and generate a new output file 
    corresponding to a particular parameter choice.
    Args:
        pwfile -  polyweight formatted .lhe file
        lhefile - name of output .lhe file
        cval - value of the parameter to use
    Options:
        nevents - number of events to process
        keep_header - whether or not to keep the .lhe headers in the new file
    
    '''
    poly = LHEfile(pwfile, nevents=nevents)    
    
    if not keep_header:
        # reset preamble
        poly.preamble = StringIO.StringIO()
        print >> poly.preamble, '<LesHouchesEvents>'
        print >> poly.preamble, '<header>'
        print >> poly.preamble, '</header>'

    new = poly.clone(lhefile)

    sumwgts = 0.
    
    for j, evt in enumerate(poly):
        
        pwgts = evt.wgts_dict()
        wgt = weight_func(cvals, pwgts)
        
        sumwgts += wgt
        evt._rwgt = None
        evt.info.XWGT = wgt
        poly.record(evt)
   
        if (j > 0) and (j % 10000 == 0): print 'Processed {} events'.format(j)
   
    print 'Generated .lhe file {} from input polyweight file {}'.format(lhefile, pwfile)
    print 'new sum of weights: {}'.format(sumwgts)

def make_polyweight_1D(partonfile, cval_sim, pwfile=None, nevents=None, keep_header=True):
    '''
    Generate a polyweight formatted .lhe file from a reweighted .lhe sample.
    The file must contain 3 weights names 'rwgt_1','rwgt_2','rwgt_3' and the 
    function assumes they correspond to the values of the parameters
    0, +cval_sim and -cval_sim respectively.
    The polyweight formatted file contains unphysical weights corresponding to 
    the polynomial coefficients p0, p1 and p2, assuming the weight depends 
    quadratically on the parameter: w = p0 + p1*c + p2*c^2
    
    Args:
        partonfile - Input .lhe file
        cval_cim - value of the parameter used in the simulation
    Options:
        pwfile - name of output .pw file. Default appends '.pw' to the input 
                 file name
        nevents - number of events to process
        keep_header - whether or not to keep the .lhe headers in the new file
    '''
    parton = LHEfile(partonfile, nevents=nevents)    
    
    
    if pwfile is None:
        pwfile = partonfile +'.pw'
        
    newinitrwgt = ET.Element('initrwgt')
    newinitrwgt.text='\n'
    newinitrwgt.tail='\n'
    newwgtgrp = ET.SubElement(newinitrwgt, 'polyweights')
    newwgtgrp.text='\n'
    newwgtgrp.tail='\n'
    p0_el = ET.SubElement(newwgtgrp, 'weight', attrib={'id':'p0'})
    p0_el.text='Constant term\n'
    p0_el.tail='\n'
    p1_el = ET.SubElement(newwgtgrp, 'weight', attrib={'id':'p1'})
    p1_el.text='Linear term coefficient\n'
    p1_el.tail='\n'
    p2_el = ET.SubElement(newwgtgrp, 'weight', attrib={'id':'p2'})
    p2_el.text='Quadratic term coefficient\n'
    p2_el.tail='\n'
    
    parton.initrwgt = newinitrwgt
    
    if not keep_header:
        # reset preamble
        parton.preamble = StringIO.StringIO()
        print >> parton.preamble, '<LesHouchesEvents>'        
        print >> parton.preamble, '<header>'    
        print >> parton.preamble, ET.tostring(newinitrwgt)    
        print >> parton.preamble, '</header>'
    else:
        # modify preamble in place
        newpreamble = StringIO.StringIO()
        # print parton.preamble.getvalue()
        preamble_iter =  StringIO.StringIO(parton.preamble.getvalue())
        # for line in parton.preamble:
        for line in preamble_iter:
            if line.strip().startswith('<initrwgt>'):
                while line.strip() != '</initrwgt>':
                    line = preamble_iter.next()
                print >> newpreamble, ET.tostring(newinitrwgt)
            else:
                print >> newpreamble, line.strip()
        
        parton.preamble = newpreamble
        
    poly = parton.clone(pwfile)
    
    for j, evt in enumerate(parton):
        wgts = evt.wgts_dict()
        
        p0, plus, minus = wgts['rwgt_1'], wgts['rwgt_2'], wgts['rwgt_3']
        
        p1  = (plus - minus)/(2.*cval_sim)
        
        p2 = ((plus + minus) - 2.*p0)/(2.*cval_sim**2)
        
        newrwgt = ET.Element('rwgt')
        newrwgt.text='\n'
        newrwgt.tail='\n'
        
        for pi, pid in zip((p0,p1,p2),('p0','p1','p2')):
            wgt = ET.SubElement(newrwgt, 'wgt', attrib={'id':pid})
            wgt.text = '{:.8e}'.format(pi)
            wgt.tail = '\n'
        
        evt._rwgt = newrwgt
        
        evt.info.XWGT = 1.
        
        parton.record(evt)
        
        if (j % 10000 == 0): print 'Processed {} events'.format(j)    
        # if (nevents is not None) and (j==nevents):
        #     break
        
    print 'Generated polyweight .lhe file: {}'.format(pwfile)
    
def gen_polyweight_1D(pwfile, lhefile, cval, nevents = None, keep_header=True):
    '''
    Take a polyweight formatted .lhe file and generate a new output file 
    corresponding to a particular parameter choice.
    Args:
        pwfile -  polyweight formatted .lhe file
        lhefile - name of output .lhe file
        cval - value of the parameter to use
    Options:
        nevents - number of events to process
        keep_header - whether or not to keep the .lhe headers in the new file
    
    '''
    poly = LHEfile(pwfile, nevents=nevents)    
    
    if not keep_header:
        # reset preamble
        poly.preamble = StringIO.StringIO()
        print >> poly.preamble, '<LesHouchesEvents>'        
        print >> poly.preamble, '<header>'        
        print >> poly.preamble, '</header>'
    
    new = poly.clone(lhefile)
    
    sumwgts = 0.

    for j, evt in enumerate(poly):
        wgts = evt.wgts_dict()
        wgt = 0.
        for exponent in range(4):
            coeff = wgts.get('p{}'.format(exponent), 0.)
            wgt += coeff*cval**exponent
        
        sumwgts += wgt
        evt._rwgt = None
        evt.info.XWGT = wgt
        poly.record(evt)

        if (j % 10000 == 0): print 'Processed {} events'.format(j)   
        
        # if (nevents is not None) and (j==nevents):
        #     print 'whuut'
        #     break
            
    print 'Generated .lhe file {} from input polyweight file {}'.format(lhefile, pwfile)
    print 'new sum of weights: {}'.format(sumwgts)       
        
    

if __name__=="__main__":
    pfile = sys.argv[1]
    # make_polyweight(pfile, 0.03, nevents = None)
    gen_polyweight(pfile, 'VBF_parton_50k_+003.lhe', 0.03, keep_header=False)
    gen_polyweight(pfile, 'VBF_parton_50k_-003.lhe', -0.03, keep_header=False)
    gen_polyweight(pfile, 'VBF_parton_50k_SM.lhe', 0., keep_header=False)
    