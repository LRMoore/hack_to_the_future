#!/usr/bin/env python
import sys, os
sys.path.append('{}/../'.format(os.path.dirname(__file__)))
os.chdir(os.path.dirname(__file__))

from itertools import izip
from pyLHE.functions import reweight
from pyLHE import LHEfile

# reweight(lhefile, wgt_id, nevents=None, keep_header=True, outputfile=None)

# def test_reweight():
reweight('sample_pw.lhe.pw', 'p1', nevents=10, keep_header=False)
    
assert os.path.exists('sample_pw_p1.lhe')

infile = LHEfile('sample_pw.lhe', nevents=10)
outfile = LHEfile('sample_pw_p1.lhe')

wgt_ind = infile.wgts_id().index('rwgt_1')
for j, (ievt,oevt) in enumerate(izip(infile,outfile)):
    assert ievt.wgts()[wgt_ind] ==  oevt.wgt()
    
# os.remove(outfile.name)


# reweight('sample_pw.lhe', 'rwgt_4', nevents=10, keep_header=False)
    
    
    
