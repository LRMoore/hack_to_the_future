#!/usr/bin/env python
import sys, os
sys.path.append('{}/../'.format(os.path.dirname(__file__)))
os.chdir(os.path.dirname(__file__))

from itertools import izip
from pyLHE.polyweight import make_polyweight
from pyLHE import LHEfile

make_polyweight('sample_pw.lhe', 0.03, nevents=10, keep_header=True)


