from pyLHE import LHEfile
import StringIO
from itertools import izip
################################################################################
# Useful methods
def reweight_from_parton(partonfile, recofile, nevents=None, keep_header=True):
    ''' 
    Duplicate a reco level LHE file according to a parton level LHE file which 
    includes reweighting information. The function will generate N new 
    reweighted LHE files where N is the number of new weights specified in the 
    FIRST <weightgroup> category defined in the <initrwgt> block. The new 
    weights are read from the <rwgt> block per event. The new LHE files are 
    named according to the 'id' attribute of each weight by inserting '_wgtID' 
    before the .lhe suffix of the original file name.
    '''
    #
    assert recofile.endswith('.lhe'), 'Filenames must end with .lhe'
    
    parton = LHEfile(partonfile, nevents=nevents)
    reco = LHEfile(recofile, nevents=nevents)
    
    if not keep_header:
        # reset preamble
        reco.preamble = StringIO.StringIO()
        print >> reco.preamble, '<LesHouchesEvents>'        
        print >> reco.preamble, '<header>'        
        print >> reco.preamble, '</header>'        

    filenames = [reco.name.replace('.lhe', '_{}.lhe'.format(x))
                 for x in parton.wgts_id()]
    clones = [reco.clone(fn) for fn in filenames]
        
    sumwgts = [0. for _ in filenames]
    
    for j,(pevt, revt) in enumerate(izip(parton, reco)):
        for i, wgt in enumerate(pevt.wgts()):
            revt.info.XWGT = wgt
            reco.record(revt, i)
            sumwgts[i] += wgt 
        if (j % 10000 == 0): print 'Processed {} events'.format(j)   
        
    print 'Generated {} duplicate lhe files:'.format(len(filenames))
    print 'filename,\t new sum of weights:'
    for swgt, fn in zip(sumwgts, filenames):
        print '{}\t{}'.format(fn, swgt)

def reweight(lhefile, wgt_id, nevents=None, keep_header=True, outputfile=None):
    ''' 
    Reweight an .lhe formatted file according to the weight stored in the 
    wgt_id tag in the rwgt.
    '''
    #
    assert (lhefile.endswith('.lhe') or lhefile.endswith('.lhe.pw')), \
           'Filename must end with .lhe or .lhe.pw'
    
    myfile = LHEfile(lhefile, nevents=nevents)
    
    wgt_ids = myfile.wgts_id()
    
    try:
        wgt_index = wgt_ids.index(wgt_id)
    except ValueError:
        print 'function reweight(): wgt_id not found in {}'.format(lhefile)
        print 'weight id "{}" must be in {}'.format(wgt_id,wgt_ids)
        return      
    
    if outputfile is None:
        outputfile = myfile.name.replace('.lhe', '_{}.lhe'.format(wgt_id)).replace('.pw','')
    
    if not keep_header:
        # reset preamble
        myfile.preamble = StringIO.StringIO()
        print >> myfile.preamble, '<LesHouchesEvents>'        
        print >> myfile.preamble, '<header>'        
        print >> myfile.preamble, '</header>'

    clone = myfile.clone(outputfile)
    
    sumwgts = 0.
    
    for j,evt in enumerate(myfile):
        wgt = evt.wgts()[wgt_index]
        evt.info.XWGT = wgt
        myfile.record(evt)
        sumwgts += wgt 
        if (j % 10000 == 0): print 'Processed {} events'.format(j) 
    
def take_weights(partonfile, recofile, nevents=None, keep_header=True):
    ''' 
    Duplicate a reco level LHE file according to a parton level LHE file which 
    includes reweighting information. The function will generate a new 
    LHE file containing the <rwgt> block of the input parton file.
    '''
    #
    assert recofile.endswith('.lhe'), 'Filenames must end with .lhe'
    
    parton = LHEfile(partonfile, nevents=nevents)
    reco = LHEfile(recofile, nevents=nevents)
    
    if not keep_header:
        # reset preamble
        reco.preamble = StringIO.StringIO()
        print >> reco.preamble, '<LesHouchesEvents>'        
        print >> reco.preamble, '<header>'        
        print >> reco.preamble, '</header>'        

    filename = reco.name.replace('.lhe', '_rwgt.lhe')

    clones = reco.clone(filename)
            
    for j,(pevt, revt) in enumerate(izip(parton, reco)):
        revt._rwgt = pevt._rwgt
        reco.record(revt)
        if (j % 10000 == 0): print 'Processed {} events'.format(j)    
    print 'Generated duplicate lhe file: ' + filename

def concatenate(outputfile, *inputfiles):
    ''' 
    Concatenate a list of input LHE files into a single outputfile.
    '''
    
    infiles = [LHEfile(f) for f in inputfiles]

    outfile = open(outputfile, 'w')
    outfile.write(infiles[0].preamble.getvalue())
    outfile.write(str(infiles[0].init))

    print 'Concatenating {} lhe files'.format(len(infiles))

    ctr=0
    for f in infiles:
        for evt in f:
            outfile.write(str(evt))
            ctr+=1

    print 'Wrote {} events into lhe file: {}'.format(ctr, outputfile)
    
    
    
    
    
    
    


    