#!/usr/bin/env python
################################################################################
import os, argparse, sys, re
import subprocess as sub
from exe import shower
from collections import OrderedDict
################################################################################
parser = argparse.ArgumentParser(description="Shower LHE events with Pythia 8.")
parser.add_argument('inputfile', metavar='INPUTFILE', type=str)
parser.add_argument("-n","--nevents", metavar="NEVENTS", type=int, default=None, 
                    help="Number of LHE events to shower")
parser.add_argument("-o","--output", metavar="OUTPUTFILE", type=str, default=None, 
                    help="Specify output file.")
parser.add_argument("--PDF", metavar="PDFSET", type=str, default="CT10", 
                    help="Which PDF set from LHAPDF6 to use")
parser.add_argument("--hadronize", action="store_true", 
                    help="Switch hadronisation on")
parser.add_argument("-d","--decay", metavar="DECAYS", nargs='*', 
                    type=int, action='append', default = [],
                    help=("Specify a particle for Pythia to decay using PDG "
                          "IDs. The first ID is the particle to be decayed and "
                          "the following IDs should match the decay products "
                          "as required by the onIfMatch option.") )
parser.add_argument("--option", metavar="OPT", type=str, default=[], nargs='+',
                    help=("Feed additional options to Pythia. This can either be"
                    "strings or file names containing the options."))
################################################################################
args = parser.parse_args()
################################################################################
options = OrderedDict([
           ('PDF:pSet', 'LHAPDF6:{}'.format(args.PDF.strip())),
           ('HadronLevel:Hadronize', 'on' if args.hadronize else 'off')
           ])

for decay in args.decay:
    try:
        moth, child = decay[0], [str(x) for x in decay[1:]]
        if len(child) < 2:
            raise IndexError
        dopt = '{}:onIfMatch'.format(moth)
        if dopt not in options:
            options['{}:onMode'.format(moth)] = 'off'
        options[dopt] = ' '.join(child)
        
    except IndexError:
        print 'shower: skipped decay option "--decay {}"'.format(decay)
################################################################################
def getopt(line):
    match = optre.match(line)
    try:
        opt, val = match.group(1).strip(), match.group(2).strip()
        if opt not in options:
            options[opt] = val
        else:
            oldval = options[opt]
            if oldval!=val:
                print ('shower.py: Ignored option {}={} given in '
                       '--option as it clashes with a previously '
                       'specified value of {}').format(opt, val, oldval)
    except AttributeError:
        pass

comment = ['#', '!', '/*', '*/']
for option in args.option:
    optre = re.compile(r'\s*(.*)=(.*)\s*')
    # See if it is a file
    if os.path.exists(option):
        iteropt = iter(open(option, 'r'))
        try:
            while True:
                sline = iteropt.next().strip()
                # pythia style comments
                if sline.startswith('/*'):
                    while not '*/' in sline:
                        sline = iteropt.next().strip()
                # any other comments
                is_comment = any([sline.startswith(x) for x in comment])
                if not (is_comment or sline=='' or '*/' in sline): 
                    getopt(sline)
        except StopIteration:
            pass            

    else:
        getopt(option.strip())
################################################################################
# call Pythia exectuable
shower(args.nevents, args.inputfile, args.output, options)
################################################################################

        