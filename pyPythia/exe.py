import subprocess as sub
import tempfile 
import settings
import os

def shower(nevents, infile, outfile, options):
    '''
    Wrapper for Pythia 8 executable bin/shower. Source code found in 
    src/shower.cc. Initialises Pythia 8 reading from a temporary option file 
    created using the contents of options. nevents events from an LHE file 
    infile is showered using those settings, outputting a HepMC file outfile.
    '''
    # check args
    assert os.path.exists(infile), 'shower: input LHE file not found'
    if outfile is None: 
        outfile = infile.replace('.lhe', '.hepmc')
    if outfile == infile:
        outfile = 'out_'+outfile
    if nevents is None:
        nevents = count_events(infile)
    
    defaults = [(o, v) for o, v in settings.DEFAULT_OPTS if o not in options]
    options.update(defaults)
    options = '''
  {}
'''.format('\n  '.join([x + ' = ' + y for x,y 
                          in options.iteritems()]))
    
    # temporary option file
    opthandle, optpath = tempfile.mkstemp(suffix='.pythia.input', 
                                          dir=os.getcwd())
    optfile = os.fdopen(opthandle, 'w')
    optfile.write(options)
    optfile.close()
    
    # log file
    with open(outfile+'.log', 'w') as log:

        exe = sub.Popen([settings.MAIN, str(nevents), 
                         optpath, infile, outfile],
                         stdout=log, stderr=sub.STDOUT)
        log.write('{:#^80}'.format(' Options read by PYTHIA 8 '))
        log.write(options)
        log.write('{:#^80}\n'.format(' PYTHIA 8 output from shower.py '))
        log.write('shower: {} HepMC to be generated.\n'.format(nevents))
    
    exe.wait()
    
    sub.call(['rm', optpath])

def count_events(lhefile):
    ctr = 0
    for line in open(lhefile, 'r'):
        if '<event>' in line and not line.startswith('#'):
            ctr+=1
    return ctr
