// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Example for Les Houches 2015
// Original Author: Mikhail Kirsanov, Mikhail.Kirsanov@cern.ch
// This program illustrates how HepMC can be interfaced to Pythia8.
// It studies the charged multiplicity distribution at the LHC.
// HepMC events are output to the hepmcout41.dat file.

// WARNING: typically one needs 25 MB/100 events at the LHC.
// Therefore large event samples may be impractical.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
// #include "<stdlib.h>"
// #include "Pythia8Plugins/LHAPDF6.h"

using namespace Pythia8;

int main(int argc, char** argv) {

  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Parse args
  int nevents;
  string fileIn="", fileOut="", settings="";
  string usage = "Usage: ./shower NEVENTS OPTIONFILE INPUTFILE [OUTPUTFILE]\n";
  if(argc >= 4) { 
      nevents = atoi(argv[1]); // Integer for number of events
      settings = argv[2]; // file containing pythia options
      fileIn = argv[3]; // .lhe file  to shower/hadronize
  } 
  // Too few arguments
  else {
      cout << usage; 
      return 0;
  }
  // Optional name of output file (will be suffixed by '.hepmc')
  if(argc == 5) fileOut = argv[4]; 
  // Too many arguments
  if(argc > 5){cout << usage; 
      return 0;
  }
  
  cout << "shower: Reading settings from " << settings << ".";

  // Init
  Pythia pythia;
  
  // Read option file
  pythia.readFile(settings);
  
  // Specify LHE format
  pythia.readString("Beams:frameType = 4");
  
  // Set input file
  stringstream ss;
  ss << "Beams:LHEF = " << fileIn;
  pythia.readString(ss.str());

  // Specify file where HepMC events will be stored.
  if (not fileOut.length()) fileOut = fileIn+".hepmc";
  HepMC::IO_GenEvent ascii_io(fileOut, std::ios::out);

  pythia.init();
  
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nevents; ++iEvent) {
    if (!pythia.next()) continue;

    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build; but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;

  // End of event loop. Statistics.
  }
  pythia.stat();

  // Done.
  return 0;
}
