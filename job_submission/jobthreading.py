#!/usr/bin/env python
#takes a text file of lots of commands and executes 'njobs' of them in 'ncores' threads removing the commands from the file
from parallel_run import *
import optparse,subprocess

parser=optparse.OptionParser('Usage: %prog commands.txt njobs ncores')
(opt,args)=parser.parse_args()

with open(args[0],'r') as f:
      lines=f.readlines()
jobs=[j.strip() for j in lines]
job_arg=int(args[1])
njobs=job_arg if job_arg!=0 else len(jobs)

myjobs, comments=[],[]
for j in range(njobs):
      try:
            while True:
                job=jobs.pop().strip()
                if job and not job.startswith('#'):
                    myjobs.append(job)
                    break
                else:
                    comments.append(job)
      except IndexError as e:
            break
with open(args[0],'w') as f:
      f.write('\n'.join(jobs+comments))
      
def runcall(n):
 def inner():
   subprocess.check_call(n, shell=True)
 return inner

print 'running', myjobs
res=parallel_run([runcall(x) for x in myjobs],int(args[2]))
print res
