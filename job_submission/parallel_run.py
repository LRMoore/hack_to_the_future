#!/usr/bin/env python
import threading

def parallel_run(tasks, n=4):
  queue_lock = threading.Lock()
  queue = list(enumerate(tasks))
  result_lock = threading.Lock()
  result = list()
  
  def thread_main():
    while True:
      queue_lock.acquire()
      try:
        if not queue:
          return
        else:
          job = queue.pop()
      finally:
        queue_lock.release()
      
      try:
        job_result = (True, job[1]())
      except Exception, ex:
        job_result = (False, ex)
      
      result_lock.acquire()
      try:
        result.append(job_result)
      finally:
        result_lock.release()
  
  threads = []
  for i in xrange(n):
    t = threading.Thread(target=thread_main)
    t.start()
    threads.append(t)
    
  for t in threads:
    t.join()
    
  return result
  
# if __name__ == '__main__':
#   import subprocess
#   
# #  print 'CPUs:', multiprocessing.cpu_count()
#   
#   def cb(n):
#     def inner():
#       subprocess.check_call(['echo', str(n)])
#       return n
#     return inner
# 
#   print parallel_run([cb(i) for i in xrange(10)])
#   