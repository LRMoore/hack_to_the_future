#!/usr/bin/env python
import subprocess as sub
import sys
import os
import re
from tempfile import mkdtemp


def set_libpcre():
    '''Sort out finding libpcre in LD_LIBRARY_PATH.
Returns a tuple containing the directory in which libpcre.so was found or 
linked and a boolean value for whether or not a temporary directory was 
created in the process (so you can delete it if you want).'''
    # check environment for presence of this dumb library
    results = {}
    for i in sub.check_output('ldconfig -p | grep libpcre.so', shell=True).split('\n'):    
        libmatch = re.match(r'\s*(\S+)\s+\(.*\)\s+=>\s+(\S+)\s*', i)
    
        if libmatch:
            libname, libpath = libmatch.group(1), libmatch.group(2)
        else:
            continue

        results[libname] = libpath

    # add to env or make a temp dir with a symlink to one with a similar name & pray
    ld_library_path = os.environ.get('LD_LIBRARY_PATH').split(':')
    curdir = os.getcwd()

    try: 
        # try library with the exact name
        libdir = os.path.dirname(results['libpcre.so.0'])
        if libdir in ld_library_path: 
        # directory already in $LD_LIBRARY_PATH
            print 'libpcre.so.0 directory "{}" found in $LD_LIBRARY_PATH'.format(libdir)
            return libdir, False
        else:
        # add directory to $LD_LIBRARY_PATH
            os.environ['LD_LIBRARY_PATH'] = ':'.join(ld_library_path + [ libdir ] )
            print 'libpcre.so.0 directory "{}" added to $LD_LIBRARY_PATH'.format(libdir)
            return libdir, False

    except KeyError:
        # otherwise try to find a similar one
        for lib, path in results.iteritems():
            if 'libpcre.so' in lib:
                tmpdir = mkdtemp(prefix='tmp', dir=curdir)
                # make a symlink to that library with the right name
                os.symlink(path, tmpdir+'/libpcre.so.0')
                # add current directory to $LD_LIBRARY_PATH
                os.environ['LD_LIBRARY_PATH'] = ':'.join(ld_library_path + [ tmpdir ] )
                return tmpdir, True

    # when all fails
    return None, False
