#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: ./extract_coeff_dependence.py <options> <process directory 1> (<process directory 2> ...)

Given a MG5 process directory (tested only for LO events atm) with standard files:

<process directory>/Cards/param_card.dat                (all couplings in model)
<process directory>/Source/MODEL/couplings<X=1,2...>.f  (couplings called in matrix element)
<process directory>/Source/MODEL/param_write.inc        (python/UFO <-> fortran correspondences)

This returns a list of (all) couplings which appear in the matrix element and belong to an
LHA block specified by the --lha-block option (default d6). Also functionality for filtering out
couplings which appear together with a dummy parameter of choice (--filter option) which can be
given value = 1 in param card to give some flexibility. Hardcoded restrictions for some SMEFT
applications are also implemented as options, eliminating coefficients of particular operator types. 

"""

# needed for enhanced regex functionality; sudo pip install regex
import regex as re
#import re
import os
import sys
import yoda
import optparse
import shutil
import glob
import operator
import imp

# extract the set of parameters which appear in matrix elements in a madgraph process directory

# take directory name(s) as command line arg
# take filter parameter(s) to require present as an option (e.g. Lambda/delta for EFT coeffs)
# compare params called in matrix element (couplingsX.f) with the whole param card and eliminate those which don't contribute
# write the output to file 'dependent_coeffs.txt'

# test_module = imp.load_source("test_module","test_module.py")
# test_module.whatever()
#
# use imp.load_source()

op = optparse.OptionParser()
op.add_option("--filter", dest="filt", type=str, default="delta", help="Name of dummy filter parameter which accompanies the couplings of interest")
op.add_option("--lha-block", dest="block", type=str, default="d6", help="Name of lha block containing parameters of interest")
op.add_option("--ignore-rescale", dest="ignore_rescale", action="store_true", default=True, help="Switch specifying whether to ignore parameters which only rescale SM")
op.add_option("--ignore-gluons", dest="ignore_gluon", action="store_true", default=False, help="Switch specifying whether to ignore parameters which modify gluon couplings")
op.add_option("--ignore-EDMs", dest="ignore_EDMs", action="store_true", default=False, help="Switch specifying whether to ignore parameters which generate EDMs")
op.add_option("--ignore-yuk", dest="ignore_cpodd_psi2phi3", action="store_true", default=False, help="Switch specifying whether to ignore CP-odd yukawa-type operator coefficients")

# supply madgraph process directories as arguments
opts, dirs = op.parse_args()    #matching_couplings.extend(found)

# get filter param name and lha block name
filt=opts.filt
lha_block_name = opts.block

# get current working directory
maindir = os.getcwd()

# define common path from process directory to folder containing couplings
couplings_file_regex = re.compile('couplings([0-9]+).f')

# compile a regex pattern for coupling definitions
parameter_def_regex = re.compile('(GC_[0-9]+\s=\s)((.|\r\n|\r|\n)+?)(GC_[0-9]+|END)')

# regex patterns for isolating block of interest in param card
param_block_regex = re.compile('(Block\s%s\s+)(.|\n|\r|(\r|\n))*?(\s+#{3,})+?' % lha_block_name, re.IGNORECASE)

# individual param definition regex in param card
param_def_regex = '[0-9]{1,3}\s[0-9]\.[0-9]{6}e(\+|\-)[0-9]{2}\s\#\s(\w+)'

# fortran defs
fortran_def_regex = '      WRITE\(\*,\*\) \'\w+_(\w+) = \', (MDL_\w+)'

# I enter wcoeffs which renormalize SM params only in by hand here (SMEFT specific) since these will show up everywhere
# then can choose to ignore them for cp-odd fit while still using the same model
rescale_SM_coeffs = ['CPhiG','CPhiW','CPhiB','CPhiWB','CPhi','CPhiDAl','CPhiD']

# enter those associated with gluon couplings (by hand)
gluon_coeffs = ['imcdg22','cgdual','imcug33','imcdg11','imcdg33','imcug22','imcug11','cphigdual']

# enter those associated with electric dipole moments
EDM_coeffs = ['imcdb33','imcug11','imcuw22','imcew11','imcug22','imcdg33','imceb22','imcub33','imcdw33','imcdb22','imcdw11','imcuw11','imcug33','imcdg11','imcub11','imcuw33','imcew22','imceb11','imcdg22','imcub22','imcdw22','imcdb11']

# cp-odd yukawa type coeffs
psi2phi3_coeffs = ['imcdphi11','imcdphi33','imcuphi22','imcephi22','imcdphi22','imcuphi11','imcephi11']

# list of params to ignore
ignore_coeffs = []

# get rid of rescale only coefficients
if opts.ignore_rescale:
    print 'Ignoring rescale parameters...'
    ignore_coeffs.extend(rescale_SM_coeffs)

# get rid of gluon couplings
if opts.ignore_gluon:
    print 'Ignoring gluon couplings...'
    ignore_coeffs.extend(gluon_coeffs)

# get rid of gluon couplings
if opts.ignore_EDMs:
    print 'Ignoring EDM-type couplings...'
    ignore_coeffs.extend(EDM_coeffs)

# get rid of gluon couplings
if opts.ignore_cpodd_psi2phi3:
    print 'Ignoring CP-odd Yukawa-type couplings...'
    ignore_coeffs.extend(psi2phi3_coeffs)





# initialise list to store the couplings of interest
all_couplings = []

# do for each directory in arguments
for procdir in dirs:    #if procdir is a directory

    # get the full path to the couplings folder of each process directory
    couplings_path = "{x}/{y}/{z}".format(x=maindir,y=procdir,z='Source/MODEL/')
    # cards path likewise
    cards_path = "{x}/{y}/{z}".format(x=maindir,y=procdir,z='Cards/')

    # get rid of any double slashes in paths
    couplings_path = re.sub("//", "/", couplings_path)
    cards_path = re.sub("//", "/", cards_path)

    # look for matching coupling files
    #print cards_path

    # initialise list to store names of coupling files present, and set full param card path
    coupling_files = []
    param_card_file = "{x}param_card.dat".format(x=cards_path)
    fortran_params_file = "{x}param_write.inc".format(x=couplings_path)

    #
    ## look at each file in Source/MODEL/
    for (dirpath, dirnames, filenames) in os.walk(couplings_path):

        # get a list of the couplingsX.f files in here
        coupling_files.extend( filter(lambda x: re.match(couplings_file_regex,x), filenames) )

        # append the base directory path
        coupling_files = map(lambda x: "{d}{f}".format(d=dirpath,f=x), coupling_files)

        #
        ## if we don't find any couplingsX.f, exit with an error message
        if coupling_files == []:
                print 'No files entitled couplingsX.f in specified directory %s' % dirpath
                exit(1);
        ##
        #

        # exit loop after one step of walk
        break

    ##
    #

    # init list to hold param definitions
    matching_couplings = [];

    #
    ## for each couplings file identified
    for coupling_file in coupling_files:

        # open each file in read mode
        with open(coupling_file, "r") as text_file:

            # store the text as a variable
            file_text = text_file.read()

            # regex match GC_X param defs
            try:
                found = map( lambda x: x[1], re.findall(parameter_def_regex, file_text, overlapped=True) )
                matching_couplings.extend(found)

            except AttributeError:
                # not found in the file
                print 'No couplings in file: %s' % coupling_file

        ##
        #

    ##
    #

    # need to now get the fortran to python correspondence of desired parameter var names

    # look in param card
    with open(param_card_file, "r") as text_file:

        # store the text as a variable
        file_text = text_file.read()
        #print file_text

        # regex match GC_X param defs
        try:
            # get param block if interest
            block = re.search(param_block_regex, file_text)

            # turn block of text into list of matching individual param definitions
            coupling_definitions = re.findall(param_def_regex, block.group(0))
            coupling_params = map( lambda x: x[1], coupling_definitions )

        # warning if not found
        except AttributeError:
            # not found in the file
            print 'No such block {x} in file: {y}'.format(x=lha_block_name,y=param_card_file)

    ##
    #

    # now parse fortran param.inc and match against the extracted couplings
    with open(fortran_params_file, "r") as text_file:

        # store text_file
        file_text = text_file.read()

        # regex match python <-> fortran param correspondences
        try:
            # get tuples for all correspondences like mdl_<UFO param name> = ... MDL_C<FORTRAN name>
            fortran_ext_defs = re.findall(fortran_def_regex, file_text)
            # now filter this by extracting those containing the params of interest extracted from couplings.f



            #fortran_ext_defs = map( lambda x: x[1], fortran_ext_defs)

        # warning
        except AttributeError:
            # no defs of this form in file_text
            print 'No such fortran definitions found in file: {x}'.format(x=fortran_params_file)

        ##
        #

    # get the python and fortran parts of associations of variable names
    python_par_names = [y[0] for y in fortran_ext_defs]
    fortran_par_names = [y[1] for y in fortran_ext_defs]

    # build regex pattern for any expression containing the filter parameter (FORTRAN)
    fortran_filt = filter(lambda x: re.match(x[0],filt), fortran_ext_defs)[0][1]
    with_filter_regex = re.compile('.*{x}.*'.format(x=fortran_filt))

    # refine the list of couplings in the matrix element to only those in which this parameter appears
    matching_couplings = filter(lambda x: re.match(with_filter_regex,x), matching_couplings)

    # exclude cases where a shorter distinct parameter will match (e.g. CG, CGdual)
    fortran_ext_defs = map(lambda x: (x[0], '(' + x[1] + ')[^0-9a-zA-Z]'), fortran_ext_defs)

    # put together all the couplings extracted into one string
    combined_coups = reduce( lambda x, y: x+y, matching_couplings)

    # now filter out names of the analogous python params which are present in the fortran couplings called for matrix element
    used_params = [x[0] for x in filter( lambda x: re.search(x[1],combined_coups), fortran_ext_defs )]

    # now just need to get rid of those which aren't in the block of interest, and remove the dummy filter parameter
    applicable_couplings = list(set(map(lambda x: x.lower(),coupling_params)) & set(map(lambda x: x.lower(),used_params)) - set([filt]))

    #print coupling_params

    # also those which are just rescaling SM parameters (for CP-odd study we assume these are zero but they will always show up in matrix elements anyway)
    applicable_couplings = list(set(applicable_couplings) - set(map(lambda x: x.lower(),ignore_coeffs)))

    # add these to the list of all couplings (for multiple arguments)
    all_couplings.extend(applicable_couplings)
    #

    ##
    #

    #print fortran_ext_defs[len(fortran_ext_defs),2]
    #print [x[1] for x in fortran_ext_defs]
    #print matching_couplings
    #print fortran_ext_defsxz


##
#

# delete duplicates
all_couplings = list(set(all_couplings))

# quick ref print output
print 'The %s applicable couplings are: ' % str(len(all_couplings)), all_couplings

# write to file
output = "dependent_coeffs.txt"

with open(output, "w") as text_file:
    text_file.write('The %s couplings appearing in the process directories:\n\n' % str(len(all_couplings)))
    text_file.write('\n'.join(dirs))
    text_file.write('\n\nsubject to the options:\n\n')
    text_file.write('Ignore coefficients giving modified gluon couplings: ' + str(opts.ignore_gluon) + '\n')
    text_file.write('Ignore coefficients which rescale SM couplings (True for CP-odd): ' + str(opts.ignore_rescale) + '\n')
    text_file.write('Ignore coefficients giving rise to EDMs: ' + str(opts.ignore_EDMs) + '\n')
    text_file.write('Ignore CP-odd Yukawa-type couplings: ' + str(opts.ignore_cpodd_psi2phi3) + '\n')
    text_file.write('\nare:\n\n')
    text_file.write('\n'.join(all_couplings))
